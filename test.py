import torch

def calculate_node_counts(combinatorial_complex_lists):
    # 计算每个组合复合体的不同节点个数
    node_counts = [len(set(complex[0])) for complex in combinatorial_complex_lists.values()]
    # 将列表转换为[E, C]维度的张量
    node_counts_tensor = torch.tensor(node_counts).view(-1, 1)
    return node_counts_tensor

def calculate_edge_counts(combinatorial_complex_lists):
    # 计算每个组合复合体的不同节点个数
    node_counts = [len(set(complex[0])) - 1 for complex in combinatorial_complex_lists.values()]
    # 将列表转换为[E, C]维度的张量
    node_counts_tensor = torch.tensor(node_counts).view(-1, 1)
    return node_counts_tensor

# 组合复合体字典
combinatorial_complex_lists = {
    'head': [(3, 2)],
    'neck': [(2, 20)],
    'abdomen': [(8, 20, 4, 1, 16, 0, 12)],
    # ... 其他组合复合体
}

# 调用函数并打印结果
node_counts_tensor = calculate_node_counts(combinatorial_complex_lists)
edge_counts_tensor = calculate_edge_counts(combinatorial_complex_lists)

print("节点个数张量:", node_counts_tensor)
print("边数张量:", edge_counts_tensor)
