import math
# import pdb
# from termios import VINTR

import numpy as np
import torch
import torch.nn as nn
from thop import profile
# from torch.autograd import Variable
import torch.nn.functional as F
from timm.models.layers import trunc_normal_, DropPath
# from timm.models.registry import register_model
from torch import Tensor
from typing import Tuple


def import_class(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


class qkv_transform(nn.Conv1d):
    """Conv1d for qkv_transform"""


class PositionEmbeddingSine(nn.Module):
    def __init__(self, numPositionFeatures: int = 64, temperature: int = 10000, normalize: bool = True,
                 scale: float = None):
        super(PositionEmbeddingSine, self).__init__()

        self.numPositionFeatures = numPositionFeatures
        self.temperature = temperature
        self.normalize = normalize

        if scale is not None and normalize is False:
            raise ValueError("normalize should be True if scale is passed")
        if scale is None:
            scale = 2 * math.pi
        self.scale = scale

    def forward(self, x: Tensor) -> Tuple[Tensor, Tensor]:
        N, _, H, W = x.shape

        mask = torch.zeros(N, H, W, dtype=torch.bool, device=x.device)
        notMask = ~mask

        yEmbed = notMask.cumsum(1)
        xEmbed = notMask.cumsum(2)

        if self.normalize:
            epsilon = 1e-6
            yEmbed = yEmbed / (yEmbed[:, -1:, :] + epsilon) * self.scale
            xEmbed = xEmbed / (xEmbed[:, :, -1:] + epsilon) * self.scale

        dimT = torch.arange(self.numPositionFeatures, dtype=torch.float32, device=x.device)
        dimT = self.temperature ** (2 * (dimT // 2) / self.numPositionFeatures)

        posX = xEmbed.unsqueeze(-1) / dimT
        posY = yEmbed.unsqueeze(-1) / dimT

        posX = torch.stack((posX[:, :, :, 0::2].sin(), posX[:, :, :, 1::2].cos()), -1).flatten(3)
        posY = torch.stack((posY[:, :, :, 0::2].sin(), posY[:, :, :, 1::2].cos()), -1).flatten(3)

        return torch.cat((posY, posX), 3).permute(0, 3, 1, 2)


class LayerNorm(nn.Module):
    r""" LayerNorm that supports two data formats: channels_last (default) or channels_first. 
    The ordering of the dimensions in the inputs. channels_last corresponds to inputs with 
    shape (batch_size, height, width, channels) while channels_first corresponds to inputs 
    with shape (batch_size, channels, height, width).
    """

    def __init__(self, normalized_shape, eps=1e-6, data_format="channels_last"):
        super().__init__()
        self.weight = nn.Parameter(torch.ones(normalized_shape))
        self.bias = nn.Parameter(torch.zeros(normalized_shape))
        self.eps = eps
        self.data_format = data_format
        if self.data_format not in ["channels_last", "channels_first"]:
            raise NotImplementedError
        self.normalized_shape = (normalized_shape,)

    def forward(self, x):
        if self.data_format == "channels_last":
            return F.layer_norm(x, self.normalized_shape, self.weight, self.bias, self.eps)
        elif self.data_format == "channels_first":
            u = x.mean(1, keepdim=True)
            s = (x - u).pow(2).mean(1, keepdim=True)
            x = (x - u) / torch.sqrt(s + self.eps)
            x = self.weight[:, None, None] * x + self.bias[:, None, None]
            return x


class AxialAttention(nn.Module):
    def __init__(self, in_channel, out_channel, num_head=8, kernel_size=56,
                 stride=1, bias=False, spatial_dim=False):
        assert (in_channel % num_head == 0) and (out_channel % num_head == 0)
        super(AxialAttention, self).__init__()
        self.in_planes = in_channel
        self.out_planes = out_channel
        self.groups = num_head
        self.group_planes = out_channel // num_head
        self.kernel_size = kernel_size
        self.stride = stride
        self.bias = bias
        self.width = spatial_dim

        # Multi-head self attention
        self.qkv_transform = qkv_transform(in_channel, out_channel * 2, kernel_size=1, stride=1,
                                           padding=0, bias=False)
        self.bn_qkv = nn.BatchNorm1d(out_channel * 2)
        self.bn_similarity = nn.BatchNorm2d(num_head * 3)

        self.bn_output = nn.BatchNorm1d(out_channel * 2)

        # Position embedding
        self.relative = nn.Parameter(torch.randn(self.group_planes * 2, kernel_size * 2 - 1), requires_grad=True)
        query_index = torch.arange(kernel_size).unsqueeze(0)
        key_index = torch.arange(kernel_size).unsqueeze(1)
        relative_index = key_index - query_index + kernel_size - 1
        self.register_buffer('flatten_index', relative_index.view(-1))
        if stride > 1:
            self.pooling = nn.AvgPool2d((stride, 1), stride=(stride, 1))

        self.reset_parameters()

    def forward(self, x):
        # pdb.set_trace()
        if self.width:
            x = x.permute(0, 2, 1, 3)
        else:
            x = x.permute(0, 3, 1, 2)  # N, C, H, W -> N, W, C, H
        N, W, C, H = x.shape
        x = x.contiguous().view(N * W, C, H)

        # Transformations
        qkv = self.bn_qkv(self.qkv_transform(x))
        q, k, v = torch.split(qkv.reshape(N * W, self.groups, self.group_planes * 2, H),
                              [self.group_planes // 2, self.group_planes // 2, self.group_planes], dim=2)

        # Calculate position embedding
        all_embeddings = torch.index_select(self.relative, 1, self.flatten_index).view(self.group_planes * 2,
                                                                                       self.kernel_size,
                                                                                       self.kernel_size)
        q_embedding, k_embedding, v_embedding = torch.split(all_embeddings,
                                                            [self.group_planes // 2, self.group_planes // 2,
                                                             self.group_planes], dim=0)

        qr = torch.einsum('bgci,cij->bgij', q, q_embedding)
        kr = torch.einsum('bgci,cij->bgij', k, k_embedding).transpose(2, 3)

        qk = torch.einsum('bgci, bgcj->bgij', q, k)

        stacked_similarity = torch.cat([qk, qr, kr], dim=1)
        stacked_similarity = self.bn_similarity(stacked_similarity).view(N * W, 3, self.groups, H, H).sum(dim=1)
        # stacked_similarity = self.bn_qr(qr) + self.bn_kr(kr) + self.bn_qk(qk)
        # (N, groups, H, H, W)
        similarity = F.softmax(stacked_similarity, dim=3)
        sv = torch.einsum('bgij,bgcj->bgci', similarity, v)
        sve = torch.einsum('bgij,cij->bgci', similarity, v_embedding)
        stacked_output = torch.cat([sv, sve], dim=-1).view(N * W, self.out_planes * 2, H)
        output = self.bn_output(stacked_output).view(N, W, self.out_planes, 2, H).sum(dim=-2)

        if self.width:
            output = output.permute(0, 2, 1, 3)
        else:
            output = output.permute(0, 2, 3, 1)

        if self.stride > 1:
            output = self.pooling(output)

        return output

    def reset_parameters(self):
        self.qkv_transform.weight.data.normal_(0, math.sqrt(1. / self.in_planes))
        # nn.init.uniform_(self.relative, -0.1, 0.1)
        nn.init.normal_(self.relative, 0., math.sqrt(1. / self.group_planes))


class AxialAttention_dynamic(nn.Module):
    def __init__(self, in_channel, out_channel, num_head=8, kernel_size=56,
                 stride=1, bias=False, spatial_dim=False):
        assert (in_channel % num_head == 0) and (out_channel % num_head == 0)
        super(AxialAttention_dynamic, self).__init__()
        self.in_planes = in_channel
        self.out_planes = out_channel
        self.groups = num_head
        self.group_planes = out_channel // num_head
        self.kernel_size = kernel_size
        self.stride = stride
        self.bias = bias
        self.width = spatial_dim

        # Multi-head self attention
        self.qkv_transform = qkv_transform(in_channel, out_channel * 2, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn_qkv = nn.BatchNorm1d(out_channel * 2)
        self.bn_similarity = nn.BatchNorm2d(num_head * 3)
        self.bn_output = nn.BatchNorm1d(out_channel * 2)

        # Priority on encoding

        ## Initial values

        self.f_qr = nn.Parameter(torch.tensor(0.1), requires_grad=False)
        self.f_kr = nn.Parameter(torch.tensor(0.1), requires_grad=False)
        self.f_sve = nn.Parameter(torch.tensor(0.1), requires_grad=False)
        self.f_sv = nn.Parameter(torch.tensor(1.0), requires_grad=False)

        # Position embedding
        self.relative = nn.Parameter(torch.randn(self.group_planes * 2, kernel_size * 2 - 1), requires_grad=True)
        query_index = torch.arange(kernel_size).unsqueeze(0)
        key_index = torch.arange(kernel_size).unsqueeze(1)
        relative_index = key_index - query_index + kernel_size - 1
        self.register_buffer('flatten_index', relative_index.view(-1))
        if stride > 1:
            self.pooling = nn.AvgPool2d((stride, 1), stride=(stride, 1))

        self.reset_parameters()
        # self.print_para()

    def forward(self, x):
        # res = x
        if self.width:
            x = x.permute(0, 2, 1, 3)
        else:
            x = x.permute(0, 3, 1, 2)  # N, V, C, T
        N, V, C, T = x.shape
        x = x.contiguous().view(N * V, C, T)

        # Transformations
        qkv = self.bn_qkv(self.qkv_transform(x))
        q, k, v = torch.split(qkv.reshape(N * V, self.groups, self.group_planes * 2, T),
                              [self.group_planes // 2, self.group_planes // 2, self.group_planes], dim=2)


        # Calculate position embedding
        all_embeddings = torch.index_select(self.relative, 1, self.flatten_index).view(self.group_planes * 2,
                                                                                       self.kernel_size,
                                                                                       self.kernel_size)
        q_embedding, k_embedding, v_embedding = torch.split(all_embeddings,
                                                            [self.group_planes // 2, self.group_planes // 2,
                                                             self.group_planes], dim=0)
        qr = torch.einsum('bgci,cij->bgij', q, q_embedding)
        kr = torch.einsum('bgci,cij->bgij', k, k_embedding).transpose(2, 3)
        qk = torch.einsum('bgci, bgcj->bgij', q, k)


        # multiply by factors
        qr = torch.mul(qr, self.f_qr)
        kr = torch.mul(kr, self.f_kr)

        stacked_similarity = torch.cat([qk, qr, kr], dim=1)
        stacked_similarity = self.bn_similarity(stacked_similarity).view(N * V, 3, self.groups, T, T).sum(dim=1)
        # stacked_similarity = self.bn_qr(qr) + self.bn_kr(kr) + self.bn_qk(qk)
        # (N, groups, T, T, V)
        similarity = F.softmax(stacked_similarity, dim=3)
        sv = torch.einsum('bgij,bgcj->bgci', similarity, v)
        sve = torch.einsum('bgij,cij->bgci', similarity, v_embedding)

        # multiply by factors
        sv = torch.mul(sv, self.f_sv)
        sve = torch.mul(sve, self.f_sve)

        stacked_output = torch.cat([sv, sve], dim=-1).view(N * V, self.out_planes * 2, T)
        output = self.bn_output(stacked_output).view(N, V, self.out_planes, 2, T).sum(dim=-2)

        if self.width:
            output = output.permute(0, 2, 1, 3)
        else:
            output = output.permute(0, 2, 3, 1)

        if self.stride > 1:
            output = self.pooling(output)

        # output += res

        return output

    def reset_parameters(self):
        self.qkv_transform.weight.data.normal_(0, math.sqrt(1. / self.in_planes))
        # nn.init.uniform_(self.relative, -0.1, 0.1)
        nn.init.normal_(self.relative, 0., math.sqrt(1. / self.group_planes))


class AxialAttention_wopos(nn.Module):
    def __init__(self, in_channel, out_channel, num_head=8, kernel_size=56,
                 stride=1, bias=False, spatial_dim=False):
        assert (in_channel % num_head == 0) and (out_channel % num_head == 0)
        super(AxialAttention_wopos, self).__init__()
        self.in_planes = in_channel
        self.out_planes = out_channel
        self.groups = num_head
        self.group_planes = out_channel // num_head
        self.kernel_size = kernel_size
        self.stride = stride
        self.bias = bias
        self.width = spatial_dim

        # Multi-head self attention
        self.qkv_transform = qkv_transform(in_channel, out_channel * 2, kernel_size=1, stride=1,
                                           padding=0, bias=False)
        self.bn_qkv = nn.BatchNorm1d(out_channel * 2)
        self.bn_similarity = nn.BatchNorm2d(num_head)

        self.bn_output = nn.BatchNorm1d(out_channel * 1)

        if stride > 1:
            self.pooling = nn.AvgPool2d((stride, 1), stride=(stride, 1))

        self.reset_parameters()

    def forward(self, x):
        if self.width:
            x = x.permute(0, 2, 1, 3)
        else:
            x = x.permute(0, 3, 1, 2)  # N, W, C, H
        N, W, C, H = x.shape
        x = x.contiguous().view(N * W, C, H)

        # Transformations
        qkv = self.bn_qkv(self.qkv_transform(x))
        q, k, v = torch.split(qkv.reshape(N * W, self.groups, self.group_planes * 2, H),
                              [self.group_planes // 2, self.group_planes // 2, self.group_planes], dim=2)

        qk = torch.einsum('bgci, bgcj->bgij', q, k)

        stacked_similarity = self.bn_similarity(qk).reshape(N * W, 1, self.groups, H, H).sum(dim=1).contiguous()

        similarity = F.softmax(stacked_similarity, dim=3)
        sv = torch.einsum('bgij,bgcj->bgci', similarity, v)

        sv = sv.reshape(N * W, self.out_planes * 1, H).contiguous()
        output = self.bn_output(sv).reshape(N, W, self.out_planes, 1, H).sum(dim=-2).contiguous()

        if self.width:
            output = output.permute(0, 2, 1, 3)
        else:
            output = output.permute(0, 2, 3, 1)

        if self.stride > 1:
            output = self.pooling(output)

        return output

    def reset_parameters(self):
        self.qkv_transform.weight.data.normal_(0, math.sqrt(1. / self.in_planes))
        # nn.init.uniform_(self.relative, -0.1, 0.1)
        # nn.init.normal_(self.relative, 0., math.sqrt(1. / self.group_planes))


class UnfoldTemporalWindows(nn.Module):
    def __init__(self, window_size, window_stride, window_dilation=[1, 1]):
        super().__init__()
        self.window_size = window_size
        self.window_stride = window_stride
        self.window_dilation = window_dilation

        # self.padding = (window_size + (window_size - 1) * (window_dilation - 1) - 1) // 2
        self.padding = 0
        self.unfold = nn.Unfold(kernel_size=(self.window_size, 1),
                                dilation=(self.window_dilation, 1),
                                stride=(self.window_stride, 1),
                                padding=(self.padding, 0))

    def forward(self, x):
        # Input shape: (N,C,T,V), out: (N,C,T,V*window_size)
        N, C, T, V = x.shape
        x = self.unfold(x)
        # Permute extra channels from window size to the graph dimension; -1 for number of windows
        x = x.view(N, C, self.window_size, -1, V).permute(0, 1, 3, 2, 4).contiguous()
        x = x.view(N, C, -1, self.window_size * V)
        return x


class unit_att(nn.Module):
    def __init__(self, in_channel, out_channel, window_size, unfold_frame_list,  stride=1, spatial_dim=False):
        super(unit_att, self).__init__()
        self.in_channel = in_channel
        self.out_channel = out_channel
        self.window_size = window_size
        self.unfold_frame_list = unfold_frame_list
        self.stride = stride
        self.spatial_dim = spatial_dim
        self.transformers = nn.ModuleList()
        branch_channels = out_channel // len(unfold_frame_list)
        for unfold_window in self.unfold_frame_list:
            self.transformers.append(
                nn.Sequential(UnfoldTemporalWindows(window_size=unfold_window, window_stride=unfold_window, window_dilation=1),
                              AxialAttention_dynamic(in_channel=self.in_channel, out_channel=self.out_channel, kernel_size=self.window_size // unfold_window,  stride=self.stride, spatial_dim=self.spatial_dim)
                )
            )
        self.down = nn.Conv2d(self.out_channel * len(unfold_frame_list), self.out_channel, kernel_size=1, padding=0, stride=(1, 1))
        self.bn = nn.BatchNorm2d(self.out_channel)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        N, _, T, V = x.shape
        y = None
        multigranularity_outs = []
        for i in range(len(self.transformers)):
            z = self.transformers[i](x)
            z = z.contiguous().view(N, self.out_channel, -1, V)
            if z.size(2) < x.size(2):
                # 计算差异维度
                diff_dim = x.size(2) - z.size(2)

                # 如果出现维度欠缺在tensor1的第三个维度上添加0/1填充
                z = torch.cat((z, torch.zeros_like(z[:, :, -diff_dim:, :])), dim=2)
                # y = torch.cat((y, torch.ones_like(y[:, :, -diff_dim:, :])), dim=2)
            y = z + y if y is not None else z
        #     multigranularity_outs.append(z)
        # y = torch.cat(multigranularity_outs, dim=1)
        # y = self.down(y)

        y = self.bn(y)
        # y += x
        y = self.relu(y)
        return y


class TSGCNext_unit(nn.Module):
    def __init__(self, in_channels, out_channels, A, coff_embedding=4, adaptive=True, residual=True,
                 layer_scale_init_value=1e-6, drop_path=0.):
        super(TSGCNext_unit, self).__init__()
        inter_channels = out_channels // coff_embedding
        self.inter_c = inter_channels
        self.out_c = out_channels
        self.in_c = in_channels
        self.adaptive = adaptive

        self.tatt1 = unit_att(in_channels, out_channels, 64, [1, 2, 4, 8, 16, 32], 1, False)

        if residual:
            self.dwconv = nn.Conv2d(in_channels, in_channels, kernel_size=(3, 1), padding=(1, 0),
                                    groups=in_channels)  # depthwise conv Twise
        else:
            self.dwconv = nn.Conv2d(in_channels, out_channels, kernel_size=(4, 1), stride=(4, 1))
            in_channels = out_channels
        self.norm = LayerNorm(in_channels, eps=1e-6)
        self.pwconv1 = nn.Linear(in_channels, 4 * in_channels)  # pointwise/1x1 convs, implemented with linear layers
        self.act = nn.GELU()

        self.PA = nn.Parameter(torch.from_numpy(A.astype(np.float32)))

        self.pwconv2 = nn.Linear(4 * in_channels, in_channels)
        self.gamma = nn.Parameter(layer_scale_init_value * torch.ones((in_channels)),
                                  requires_grad=True) if layer_scale_init_value > 0 else None
        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        if in_channels != out_channels:
            self.down = nn.Sequential(
                LayerNorm(in_channels, eps=1e-6, data_format="channels_first"),
                nn.Conv2d(in_channels, out_channels, kernel_size=(2, 1), stride=(2, 1)),
            )
        else:
            self.down = None

        self.residual = residual
        self.alpha = nn.Parameter(torch.zeros(1))

        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.Linear)):
                trunc_normal_(m.weight, std=.02)
                nn.init.constant_(m.bias, 0)

    def forward(self, x, preA=None):
        if preA != None:
            A = self.PA
        else:
            A = self.PA
        input = x
        x = self.dwconv(x)  # (N,C,T,V)
        x = x.permute(0, 2, 3, 1)  # (N, C, T, V) -> (N, T, V, C)
        x = self.norm(x)
        x = self.pwconv1(x)  # (N, T, V, 4*C)
        N, T, V, C4 = x.shape
        x = x.reshape(N, T, V, -1, 4)
        x1 = x[:, :, :, :, 0:3]
        x2 = x[:, :, :, :, 3:4]
        if self.training:
            Ak = torch.unsqueeze(A, dim=0).repeat_interleave(N, dim=0)  # .permute(0, 3, 2, 1).contiguous().view(-1,V,3)
            x1 = torch.einsum('niuk,ntkci->ntuci', Ak, x1)  # AK: [N, 3, 25, 25]   x1: [2, 16, 25, 96, 3]
        else:
            x1 = torch.einsum('iuk,ntkci->ntuci', A, x1)
        # x1 = torch.einsum('kuv,ntvck->ntuck', A,x1)
        x = torch.cat([x1, x2], dim=-1)
        x = x.reshape(N, T, V, C4)
        x = self.act(x)
        x = self.pwconv2(x)
        # if self.gamma is not None:
        #    x = self.gamma * x
        x = x.permute(0, 3, 1, 2)  # (N, H, W, C) -> (N, C, H, W)

        if self.residual:
            y = input + self.drop_path(x)
        else:
            y = self.drop_path(x)

        # if self.down != None:
        #     y = self.down(y)

        y = self.tatt1(y)  # (N, C, T, V) -> (N, C*2, T, V)

        return y, A


class TSGCNext_unit1(nn.Module):
    def __init__(self, in_channels, out_channels, A, coff_embedding=4, adaptive=True, residual=True,
                 layer_scale_init_value=1e-6, drop_path=0.):
        super(TSGCNext_unit1, self).__init__()
        inter_channels = out_channels // coff_embedding
        self.inter_c = inter_channels
        self.out_c = out_channels
        self.in_c = in_channels
        self.adaptive = adaptive

        self.tatt1 = unit_att(in_channels, out_channels, 64, [1, 2, 4, 8, 16, 32], 1, False)

        if residual:
            self.dwconv = nn.Conv2d(in_channels, in_channels, kernel_size=(3, 1), padding=(1, 0),
                                    groups=in_channels)  # depthwise conv Twise
        else:
            self.dwconv = nn.Conv2d(in_channels, out_channels, kernel_size=(4, 1), stride=(4, 1))
            in_channels = out_channels
        self.norm = LayerNorm(in_channels, eps=1e-6)
        self.pwconv1 = nn.Linear(in_channels, 4 * in_channels)  # pointwise/1x1 convs, implemented with linear layers
        self.act = nn.GELU()

        self.PA = nn.Parameter(torch.from_numpy(A.astype(np.float32)))

        self.pwconv2 = nn.Linear(4 * in_channels, in_channels)
        self.gamma = nn.Parameter(layer_scale_init_value * torch.ones((in_channels)),
                                  requires_grad=True) if layer_scale_init_value > 0 else None
        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        if in_channels != out_channels:
            self.down = nn.Sequential(
                LayerNorm(in_channels, eps=1e-6, data_format="channels_first"),
                nn.Conv2d(in_channels, out_channels, kernel_size=(2, 1), stride=(2, 1)),
            )
        else:
            self.down = None

        self.residual = residual
        self.alpha = nn.Parameter(torch.zeros(1))

        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.Linear)):
                trunc_normal_(m.weight, std=.02)
                nn.init.constant_(m.bias, 0)

    def forward(self, x, preA=None):
        if preA != None:
            A = self.PA
        else:
            A = self.PA
        input = x
        x = self.dwconv(x)  # (N,C,T,V)
        x = x.permute(0, 2, 3, 1)  # (N, C, T, V) -> (N, T, V, C)
        x = self.norm(x)
        x = self.pwconv1(x)  # (N, T, V, 4*C)
        N, T, V, C4 = x.shape
        x = x.reshape(N, T, V, -1, 4)
        x1 = x[:, :, :, :, 0:3]
        x2 = x[:, :, :, :, 3:4]
        if self.training:
            Ak = torch.unsqueeze(A, dim=0).repeat_interleave(N, dim=0)  # .permute(0, 3, 2, 1).contiguous().view(-1,V,3)
            x1 = torch.einsum('niuk,ntkci->ntuci', Ak, x1)  # AK: [N, 3, 25, 25]   x1: [2, 16, 25, 96, 3]
        else:
            x1 = torch.einsum('iuk,ntkci->ntuci', A, x1)
        # x1 = torch.einsum('kuv,ntvck->ntuck', A,x1)
        x = torch.cat([x1, x2], dim=-1)
        x = x.reshape(N, T, V, C4)
        x = self.act(x)
        x = self.pwconv2(x)
        # if self.gamma is not None:
        #    x = self.gamma * x
        x = x.permute(0, 3, 1, 2)  # (N, H, W, C) -> (N, C, H, W)

        if self.residual:
            y = input + self.drop_path(x)
        else:
            y = self.drop_path(x)

        # if self.down != None:
        #     y = self.down(y)

        y = self.tatt1(y)

        return y, A


class Model(nn.Module):
    def __init__(self, num_class=60, num_point=25, num_person=2, graph=None, graph_args=dict(), in_channels=3,
                 drop_out=0, adaptive=True, unify=None):
        super(Model, self).__init__()

        if graph is None:
            raise ValueError()
        else:
            Graph = import_class(graph)
            self.graph = Graph(**graph_args)

        A = self.graph.A  # 3,25,25
        if unify == "coco":
            from data.unifyposecode import COCO
            vindex = COCO
            A = A[:, vindex]
            A = A[:, :, vindex]
            num_point = len(vindex)
        elif unify == "ntu":
            from data.unifyposecode import NTU
            vindex = NTU
            A = A[:, vindex]
            A = A[:, :, vindex]
            num_point = len(vindex)

        self.num_class = num_class
        self.num_point = num_point
        self.data_bn = nn.BatchNorm1d(num_person * in_channels * num_point)

        base_channel = 96
        dp_rates = [x.item() for x in torch.linspace(0, drop_out, 9)]
        stem = nn.Sequential(
            nn.Conv2d(in_channels, base_channel, kernel_size=(4, 1), stride=(4, 1)),
            LayerNorm(base_channel, eps=1e-6, data_format="channels_first")
        )
        self.l1 = stem  # TSGCNext_unit(in_channels, base_channel, A, residual=False, adaptive=adaptive,drop_path=dp_rates[0])
        # 2 5 2
        self.l2 = TSGCNext_unit(base_channel, base_channel, A, adaptive=adaptive, drop_path=dp_rates[0])
        self.l3 = TSGCNext_unit(base_channel, base_channel, A, adaptive=adaptive, drop_path=dp_rates[1])
        self.l4 = TSGCNext_unit(base_channel, base_channel, A, adaptive=adaptive, drop_path=dp_rates[2])
        self.l5 = TSGCNext_unit(base_channel, base_channel * 2, A, adaptive=adaptive, drop_path=dp_rates[3])
        self.l6 = TSGCNext_unit(base_channel * 2, base_channel * 2, A, adaptive=adaptive, drop_path=dp_rates[4])
        self.l7 = TSGCNext_unit(base_channel * 2, base_channel * 2, A, adaptive=adaptive, drop_path=dp_rates[5])
        self.l8 = TSGCNext_unit(base_channel * 2, base_channel * 4, A, adaptive=adaptive, drop_path=dp_rates[6])
        self.l9 = TSGCNext_unit(base_channel * 4, base_channel * 4, A, adaptive=adaptive, drop_path=dp_rates[7])
        self.l10 = TSGCNext_unit(base_channel * 4, base_channel * 4, A, adaptive=adaptive, drop_path=dp_rates[8])

        self.encoder = TSGCNext_unit(base_channel, base_channel*2, A, adaptive=adaptive, drop_path=dp_rates[0])
        self.decoder = TSGCNext_unit1(base_channel*2, base_channel, A, adaptive=adaptive, drop_path=dp_rates[0])

        self.norm = nn.LayerNorm(base_channel, eps=1e-6)
        # self.encode = nn.Linear(base_channel*4, base_channel*4)
        self.fc = nn.Linear(base_channel, num_class)

        self.in_channels = in_channels
        nn.init.normal_(self.fc.weight, 0, math.sqrt(2. / num_class))

    def forward(self, x):
        if len(x.shape) == 3:
            N, T, VC = x.shape
            x = x.view(N, T, self.num_point, -1).permute(0, 3, 1, 2).contiguous().unsqueeze(-1)
        if self.in_channels == 2:
            x = x[:, 0:2, :, :, :]
        N, C, T, V, M = x.size()

        x = x.permute(0, 4, 3, 1, 2).contiguous().view(N, M * V * C, T)
        x = self.data_bn(x)
        x = x.view(N, M, V, C, T).permute(0, 1, 3, 4, 2).contiguous().view(N * M, C, T, V)
        x = self.l1(x)
        # x, A1 = self.l2(x)
        # x, A2 = self.l3(x, A1)
        # x, A3 = self.l4(x, A2)
        # x, A4 = self.l5(x, A3)
        # x, A5 = self.l6(x, A4)
        # x, A6 = self.l7(x, A5)
        # x, A7 = self.l8(x, A6)
        # x, A8 = self.l9(x, A7)
        # x, A9 = self.l10(x, A8)
        x, A1 = self.encoder(x)
        x, A2 = self.decoder(x, A1)

        # N*M,C,T,V
        c_new = x.size(1)
        x = x.view(N, M, c_new, -1)
        x = x.mean(3).mean(1)
        x = self.norm(x)
        # x = self.encode(x)
        return self.fc(x)


if __name__ == '__main__':
    net = Model(graph="graph.ntu_rgb_d.Graph",
                graph_args={"labeling_mode": 'spatial'}).cuda()

    ske = torch.rand([1, 3, 256, 25, 2]).cuda()

    result = net(ske)

    # # 查看网络结构图
    # with SummaryWriter(comment='DSTA-Net') as w:
    #     w.add_graph(net, (ske,))

    # 模型参数量&推理计算量
    flops, params = profile(net, inputs=(ske,))
    print('Flops: ', flops, 'Params: ', params)

    print('FLOPs&Param:' + 'GFLOPs: %.2f G, Param: %.2f M' % (flops / 1000000000.0, params / 1000000.0))
    print(result.shape)
