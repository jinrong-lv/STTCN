import math
import os
import networkx as nx
import pdb
# from tools import Visualzation

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from thop import profile
from torch.autograd import Variable

from einops import rearrange, repeat


class qkv_transform(nn.Conv1d):
    """Conv1d for qkv_transform"""


def import_class(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def conv_branch_init(conv, branches):
    weight = conv.weight
    n = weight.size(0)
    k1 = weight.size(1)
    k2 = weight.size(2)
    nn.init.normal_(weight, 0, math.sqrt(2. / (n * k1 * k2 * branches)))
    nn.init.constant_(conv.bias, 0)


def conv_init(conv):
    if conv.weight is not None:
        nn.init.kaiming_normal_(conv.weight, mode='fan_out')
    if conv.bias is not None:
        nn.init.constant_(conv.bias, 0)


def bn_init(bn, scale):
    nn.init.constant_(bn.weight, scale)
    nn.init.constant_(bn.bias, 0)


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        if hasattr(m, 'weight'):
            nn.init.kaiming_normal_(m.weight, mode='fan_out')
        if hasattr(m, 'bias') and m.bias is not None and isinstance(m.bias, torch.Tensor):
            nn.init.constant_(m.bias, 0)
    elif classname.find('BatchNorm') != -1:
        if hasattr(m, 'weight') and m.weight is not None:
            m.weight.data.normal_(1.0, 0.02)
        if hasattr(m, 'bias') and m.bias is not None:
            m.bias.data.fill_(0)


def get_all_1_hop_subgraphs(adj_matrix):
    # 获取节点数
    num_nodes = adj_matrix.shape[0]

    # 找到所有连接的节点对
    row_indices, col_indices = np.nonzero(adj_matrix)

    # 构建一个包含所有子图的列表
    all_subgraphs = []

    for node_idx in range(num_nodes):
        # 找到与当前节点相连接的节点索引
        connected_nodes = np.unique(np.concatenate((row_indices[col_indices == node_idx],
                                                    col_indices[row_indices == node_idx])))

        # 构建子图邻接矩阵
        subgraph_matrix = np.zeros_like(adj_matrix)
        subgraph_matrix[node_idx, connected_nodes] = 1
        subgraph_matrix[connected_nodes, node_idx] = 1

        all_subgraphs.append(subgraph_matrix)

    return all_subgraphs


def degree_centrality(adjacency_matrix):
    # 计算每个节点的度
    degrees = torch.sum(adjacency_matrix, dim=1)

    # 计算度中心性
    centrality = degrees.view(-1, 1)

    return centrality


def betweenness_centrality(adjacency_matrix):
    n = adjacency_matrix.size(0)

    # 初始化介数中心性
    centrality = torch.zeros(n, 1)

    for i in range(n):
        for j in range(i + 1, n):
            for k in range(n):
                if k != i and k != j:
                    centrality[k] += (torch.inverse(torch.eye(n) - adjacency_matrix)[i, k]
                                      * torch.inverse(torch.eye(n) - adjacency_matrix)[j, k])

    # 归一化介数中心性
    centrality /= ((n - 1) * (n - 2) / 2)

    return centrality


class LayerNorm(nn.Module):
    r""" LayerNorm that supports two data formats: channels_last (default) or channels_first.
    The ordering of the dimensions in the inputs. channels_last corresponds to inputs with
    shape (batch_size, height, width, channels) while channels_first corresponds to inputs
    with shape (batch_size, channels, height, width).
    """

    def __init__(self, normalized_shape, eps=1e-6, data_format="channels_last"):
        super().__init__()
        self.weight = nn.Parameter(torch.ones(normalized_shape))
        self.bias = nn.Parameter(torch.zeros(normalized_shape))
        self.eps = eps
        self.data_format = data_format
        if self.data_format not in ["channels_last", "channels_first"]:
            raise NotImplementedError
        self.normalized_shape = (normalized_shape,)

    def forward(self, x):
        if self.data_format == "channels_last":
            return F.layer_norm(x, self.normalized_shape, self.weight, self.bias, self.eps)
        elif self.data_format == "channels_first":
            u = x.mean(1, keepdim=True)
            s = (x - u).pow(2).mean(1, keepdim=True)
            x = (x - u) / torch.sqrt(s + self.eps)
            x = self.weight[:, None, None] * x + self.bias[:, None, None]
            return x


class AxialAttention(nn.Module):
    def __init__(self, in_channel, out_channel, num_head=8, kernel_size=56,
                 stride=1, bias=False, spatial_dim=False):
        assert (in_channel % num_head == 0) and (out_channel % num_head == 0)
        super(AxialAttention, self).__init__()
        self.in_planes = in_channel
        self.out_planes = out_channel
        self.groups = num_head
        self.group_planes = out_channel // num_head
        self.kernel_size = kernel_size
        self.stride = stride
        self.bias = bias
        self.width = spatial_dim

        # Multi-head self attention
        self.qkv_transform = qkv_transform(in_channel, out_channel * 2, kernel_size=1, stride=1,
                                           padding=0, bias=False)
        self.bn_qkv = nn.BatchNorm1d(out_channel * 2)
        self.bn_similarity = nn.BatchNorm2d(num_head * 3)

        self.bn_output = nn.BatchNorm1d(out_channel * 2)

        # Position embedding
        self.relative = nn.Parameter(torch.randn(self.group_planes * 2, kernel_size * 2 - 1), requires_grad=True)
        query_index = torch.arange(kernel_size).unsqueeze(0)
        key_index = torch.arange(kernel_size).unsqueeze(1)
        relative_index = key_index - query_index + kernel_size - 1
        self.register_buffer('flatten_index', relative_index.view(-1))
        if stride > 1:
            self.pooling = nn.AvgPool2d((stride, 1), stride=(stride, 1))

        self.reset_parameters()

    def forward(self, x):
        # pdb.set_trace()
        if self.width:
            x = x.permute(0, 2, 1, 3)
        else:
            x = x.permute(0, 3, 1, 2)  # N, C, H, W -> N, W, C, H
        N, W, C, H = x.shape
        x = x.contiguous().view(N * W, C, H)

        # Transformations
        qkv = self.bn_qkv(self.qkv_transform(x))
        q, k, v = torch.split(qkv.reshape(N * W, self.groups, self.group_planes * 2, H),
                              [self.group_planes // 2, self.group_planes // 2, self.group_planes], dim=2)

        # Calculate position embedding
        all_embeddings = torch.index_select(self.relative, 1, self.flatten_index).view(self.group_planes * 2,
                                                                                       self.kernel_size,
                                                                                       self.kernel_size)
        q_embedding, k_embedding, v_embedding = torch.split(all_embeddings,
                                                            [self.group_planes // 2, self.group_planes // 2,
                                                             self.group_planes], dim=0)

        qr = torch.einsum('bgci,cij->bgij', q, q_embedding)
        kr = torch.einsum('bgci,cij->bgij', k, k_embedding).transpose(2, 3)

        qk = torch.einsum('bgci, bgcj->bgij', q, k)

        stacked_similarity = torch.cat([qk, qr, kr], dim=1)
        stacked_similarity = self.bn_similarity(stacked_similarity).view(N * W, 3, self.groups, H, H).sum(dim=1)
        # stacked_similarity = self.bn_qr(qr) + self.bn_kr(kr) + self.bn_qk(qk)
        # (N, groups, H, H, W)
        similarity = F.softmax(stacked_similarity, dim=3)
        sv = torch.einsum('bgij,bgcj->bgci', similarity, v)
        sve = torch.einsum('bgij,cij->bgci', similarity, v_embedding)
        stacked_output = torch.cat([sv, sve], dim=-1).view(N * W, self.out_planes * 2, H)
        output = self.bn_output(stacked_output).view(N, W, self.out_planes, 2, H).sum(dim=-2)

        if self.width:
            output = output.permute(0, 2, 1, 3)
        else:
            output = output.permute(0, 2, 3, 1)

        if self.stride > 1:
            output = self.pooling(output)

        return output

    def reset_parameters(self):
        self.qkv_transform.weight.data.normal_(0, math.sqrt(1. / self.in_planes))
        # nn.init.uniform_(self.relative, -0.1, 0.1)
        nn.init.normal_(self.relative, 0., math.sqrt(1. / self.group_planes))


class AxialAttention_dynamic(nn.Module):
    def __init__(self, in_channel, out_channel, num_head=8, kernel_size=56,
                 stride=1, bias=False, spatial_dim=False):
        assert (in_channel % num_head == 0) and (out_channel % num_head == 0)
        super(AxialAttention_dynamic, self).__init__()
        self.in_planes = in_channel
        self.out_planes = out_channel
        self.groups = num_head
        self.group_planes = out_channel // num_head
        self.kernel_size = kernel_size
        self.stride = stride
        self.bias = bias
        self.width = spatial_dim

        # Multi-head self attention
        self.qkv_transform = qkv_transform(in_channel, out_channel * 2, kernel_size=1, stride=1,
                                           padding=0, bias=False)
        self.bn_qkv = nn.BatchNorm1d(out_channel * 2)
        self.bn_similarity = nn.BatchNorm2d(num_head * 3)
        self.bn_output = nn.BatchNorm1d(out_channel * 2)

        # Priority on encoding

        ## Initial values

        self.f_qr = nn.Parameter(torch.tensor(0.1), requires_grad=False)
        self.f_kr = nn.Parameter(torch.tensor(0.1), requires_grad=False)
        self.f_sve = nn.Parameter(torch.tensor(0.1), requires_grad=False)
        self.f_sv = nn.Parameter(torch.tensor(1.0), requires_grad=False)

        # Position embedding
        self.relative = nn.Parameter(torch.randn(self.group_planes * 2, kernel_size * 2 - 1), requires_grad=True)
        query_index = torch.arange(kernel_size).unsqueeze(0)
        key_index = torch.arange(kernel_size).unsqueeze(1)
        relative_index = key_index - query_index + kernel_size - 1
        self.register_buffer('flatten_index', relative_index.view(-1))
        if stride > 1:
            self.pooling = nn.AvgPool2d((stride, 1), stride=(stride, 1))

        self.reset_parameters()
        # self.print_para()

    def forward(self, x):
        # res = x
        if self.width:
            x = x.permute(0, 2, 1, 3)
        else:
            x = x.permute(0, 3, 1, 2)  # N, V, C, T
        N, V, C, T = x.shape
        x = x.contiguous().view(N * V, C, T)

        # Transformations
        qkv = self.bn_qkv(self.qkv_transform(x))
        q, k, v = torch.split(qkv.reshape(N * V, self.groups, self.group_planes * 2, T),
                              [self.group_planes // 2, self.group_planes // 2, self.group_planes], dim=2)


        # Calculate position embedding
        all_embeddings = torch.index_select(self.relative, 1, self.flatten_index).view(self.group_planes * 2,
                                                                                       self.kernel_size,
                                                                                       self.kernel_size)
        q_embedding, k_embedding, v_embedding = torch.split(all_embeddings,
                                                            [self.group_planes // 2, self.group_planes // 2,
                                                             self.group_planes], dim=0)
        qr = torch.einsum('bgci,cij->bgij', q, q_embedding)
        kr = torch.einsum('bgci,cij->bgij', k, k_embedding).transpose(2, 3)
        qk = torch.einsum('bgci, bgcj->bgij', q, k)


        # multiply by factors
        qr = torch.mul(qr, self.f_qr)
        kr = torch.mul(kr, self.f_kr)

        stacked_similarity = torch.cat([qk, qr, kr], dim=1)
        stacked_similarity = self.bn_similarity(stacked_similarity).view(N * V, 3, self.groups, T, T).sum(dim=1)
        # stacked_similarity = self.bn_qr(qr) + self.bn_kr(kr) + self.bn_qk(qk)
        # (N, groups, T, T, V)
        similarity = F.softmax(stacked_similarity, dim=3)
        sv = torch.einsum('bgij,bgcj->bgci', similarity, v)
        sve = torch.einsum('bgij,cij->bgci', similarity, v_embedding)

        # multiply by factors
        sv = torch.mul(sv, self.f_sv)
        sve = torch.mul(sve, self.f_sve)

        stacked_output = torch.cat([sv, sve], dim=-1).view(N * V, self.out_planes * 2, T)
        output = self.bn_output(stacked_output).view(N, V, self.out_planes, 2, T).sum(dim=-2)

        if self.width:
            output = output.permute(0, 2, 1, 3)
        else:
            output = output.permute(0, 2, 3, 1)

        if self.stride > 1:
            output = self.pooling(output)

        # output += res

        return output

    def reset_parameters(self):
        self.qkv_transform.weight.data.normal_(0, math.sqrt(1. / self.in_planes))
        # nn.init.uniform_(self.relative, -0.1, 0.1)
        nn.init.normal_(self.relative, 0., math.sqrt(1. / self.group_planes))


class AxialAttention_wopos(nn.Module):
    def __init__(self, in_channel, out_channel, num_head=8, kernel_size=56,
                 stride=1, bias=False, spatial_dim=False):
        assert (in_channel % num_head == 0) and (out_channel % num_head == 0)
        super(AxialAttention_wopos, self).__init__()
        self.in_planes = in_channel
        self.out_planes = out_channel
        self.groups = num_head
        self.group_planes = out_channel // num_head
        self.kernel_size = kernel_size
        self.stride = stride
        self.bias = bias
        self.width = spatial_dim

        # Multi-head self attention
        self.qkv_transform = qkv_transform(in_channel, out_channel * 2, kernel_size=1, stride=1,
                                           padding=0, bias=False)
        self.bn_qkv = nn.BatchNorm1d(out_channel * 2)
        self.bn_similarity = nn.BatchNorm2d(num_head)

        self.bn_output = nn.BatchNorm1d(out_channel * 1)

        if stride > 1:
            self.pooling = nn.AvgPool2d((stride, 1), stride=(stride, 1))

        self.reset_parameters()

    def forward(self, x):
        if self.width:
            x = x.permute(0, 2, 1, 3)
        else:
            x = x.permute(0, 3, 1, 2)  # N, W, C, H
        N, W, C, H = x.shape
        x = x.contiguous().view(N * W, C, H)

        # Transformations
        qkv = self.bn_qkv(self.qkv_transform(x))
        q, k, v = torch.split(qkv.reshape(N * W, self.groups, self.group_planes * 2, H),
                              [self.group_planes // 2, self.group_planes // 2, self.group_planes], dim=2)

        qk = torch.einsum('bgci, bgcj->bgij', q, k)

        stacked_similarity = self.bn_similarity(qk).reshape(N * W, 1, self.groups, H, H).sum(dim=1).contiguous()

        similarity = F.softmax(stacked_similarity, dim=3)
        sv = torch.einsum('bgij,bgcj->bgci', similarity, v)

        sv = sv.reshape(N * W, self.out_planes * 1, H).contiguous()
        output = self.bn_output(sv).reshape(N, W, self.out_planes, 1, H).sum(dim=-2).contiguous()

        if self.width:
            output = output.permute(0, 2, 1, 3)
        else:
            output = output.permute(0, 2, 3, 1)

        if self.stride > 1:
            output = self.pooling(output)

        return output

    def reset_parameters(self):
        self.qkv_transform.weight.data.normal_(0, math.sqrt(1. / self.in_planes))
        # nn.init.uniform_(self.relative, -0.1, 0.1)
        # nn.init.normal_(self.relative, 0., math.sqrt(1. / self.group_planes))


class UnfoldTemporalWindows(nn.Module):
    def __init__(self, window_size, window_stride, window_dilation=[1, 1]):
        super().__init__()
        self.window_size = window_size
        self.window_stride = window_stride
        self.window_dilation = window_dilation

        # self.padding = (window_size + (window_size - 1) * (window_dilation - 1) - 1) // 2
        self.padding = 0
        self.unfold = nn.Unfold(kernel_size=(self.window_size, 1),
                                dilation=(self.window_dilation, 1),
                                stride=(self.window_stride, 1),
                                padding=(self.padding, 0))

    def forward(self, x):
        # Input shape: (N,C,T,V), out: (N,C,T,V*window_size)
        N, C, T, V = x.shape
        x = self.unfold(x)
        # Permute extra channels from window size to the graph dimension; -1 for number of windows
        x = x.view(N, C, self.window_size, -1, V).permute(0, 1, 3, 2, 4).contiguous()
        x = x.view(N, C, -1, self.window_size * V)
        return x


class CTRGC(nn.Module):
    def __init__(self, in_channels, out_channels, rel_reduction=8, mid_reduction=1, loop_times=4, fuse_alpha=0.15):
        super(CTRGC, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        if in_channels == 3 or in_channels == 9:
            self.rel_channels = 8
            self.mid_channels = 16
        else:
            self.rel_channels = in_channels // rel_reduction
            self.mid_channels = in_channels // mid_reduction
        self.conv1 = nn.Conv2d(self.in_channels, self.rel_channels, kernel_size=1)
        self.conv2 = nn.Conv2d(self.in_channels, self.rel_channels, kernel_size=1)
        self.conv3 = nn.Conv2d(self.in_channels, self.out_channels, kernel_size=1)
        self.conv4 = nn.Conv2d(self.rel_channels, self.out_channels, kernel_size=1)
        self.tanh = nn.Tanh()
        self.relu = nn.ReLU()
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                conv_init(m)
            elif isinstance(m, nn.BatchNorm2d):
                bn_init(m, 1)

        self.loop_times = loop_times
        self.fuse_alpha = fuse_alpha

    def k_hop(self, A):
        # A: N, C, V, V
        N, C, V, _ = A.shape
        # A0: 1, 1, V, V
        A0 = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0) * self.fuse_alpha

        A_power = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0)
        for i in range(1, self.loop_times + 1):
            A_power = torch.einsum('ncuv,ncvw->ncuw', A, A_power)
            A0 = A_power * (self.fuse_alpha * (1 - self.fuse_alpha) ** i) + A0

        return A0

    def forward(self, x, A=None, alpha=1):
        x1, x2, x3 = self.conv1(x).mean(-2), self.conv2(x).mean(-2), self.conv3(x)
        x1 = self.relu(x1.unsqueeze(-1) - x2.unsqueeze(-2))
        x1 = self.conv4(x1) * alpha + (A.unsqueeze(0).unsqueeze(0) if A is not None else 0)  # N,C,V,V

        if self.loop_times != 0:
            fuse_A = self.k_hop(x1)
            x_out = torch.einsum('ncuv,nctv->nctu', fuse_A, x3)
            return x_out
        else:
            x_out = torch.einsum('ncuv,nctv->nctu', x1, x3)
            return x_out
        # x_out = torch.einsum('ncuv,nctv->nctu', x1, x3)
        return x_out


class unit_att(nn.Module):
    def __init__(self, in_channel, out_channel, window_size, unfold_frame_list,  stride=1, spatial_dim=False):
        super(unit_att, self).__init__()
        self.in_channel = in_channel
        self.out_channel = out_channel
        self.window_size = window_size
        self.unfold_frame_list = unfold_frame_list
        self.stride = stride
        self.spatial_dim = spatial_dim
        self.transformers = nn.ModuleList()
        branch_channels = out_channel // len(unfold_frame_list)
        for unfold_window in self.unfold_frame_list:
            self.transformers.append(
                nn.Sequential(UnfoldTemporalWindows(window_size=unfold_window, window_stride=unfold_window, window_dilation=1),
                              AxialAttention_dynamic(in_channel=self.in_channel, out_channel=self.out_channel, kernel_size=self.window_size // unfold_window,  stride=self.stride, spatial_dim=self.spatial_dim)
                )
            )
        self.down = nn.Conv2d(self.out_channel * len(unfold_frame_list), self.out_channel, kernel_size=1, padding=0, stride=(1, 1))
        self.bn = nn.BatchNorm2d(self.out_channel)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        N, _, T, V = x.shape
        y = None
        multigranularity_outs = []
        for i in range(len(self.transformers)):
            z = self.transformers[i](x)
            z = z.contiguous().view(N, self.out_channel, -1, V)
            if z.size(2) < x.size(2):
                # 计算差异维度
                diff_dim = x.size(2) - z.size(2)

                # 如果出现维度欠缺在tensor1的第三个维度上添加0/1填充
                z = torch.cat((z, torch.zeros_like(z[:, :, -diff_dim:, :])), dim=2)
                # y = torch.cat((y, torch.ones_like(y[:, :, -diff_dim:, :])), dim=2)
            y = z + y if y is not None else z
        #     multigranularity_outs.append(z)
        # y = torch.cat(multigranularity_outs, dim=1)
        # y = self.down(y)

        y = self.bn(y)
        # y += x
        y = self.relu(y)
        return y


class unit_gcn(nn.Module):
    def __init__(self, in_channels, out_channels, A, coff_embedding=4, adaptive=True, residual=True, **kwargs):
        super(unit_gcn, self).__init__()
        inter_channels = out_channels // coff_embedding
        self.inter_c = inter_channels
        self.out_c = out_channels
        self.in_c = in_channels
        self.adaptive = adaptive
        self.num_subset = A.shape[0]
        # self.num_subset = 3
        self.convs_subGraph = nn.ModuleList()
        self.convs = nn.ModuleList()
        for i in range(self.num_subset):
            self.convs.append(CTRGC(in_channels, out_channels, **kwargs))

        if residual:
            if in_channels != out_channels:
                self.down = nn.Sequential(
                    nn.Conv2d(in_channels, out_channels, 1),
                    nn.BatchNorm2d(out_channels)
                )
            else:
                self.down = lambda x: x
        else:
            self.down = lambda x: 0
        if self.adaptive:
            self.PA = nn.Parameter(torch.from_numpy(A.astype(np.float32)))
        else:
            self.A = Variable(torch.from_numpy(A.astype(np.float32)), requires_grad=False)
        self.alpha = nn.Parameter(torch.tensor(0.5))
        self.bn = nn.BatchNorm2d(out_channels)
        self.soft = nn.Softmax(-2)
        self.relu = nn.ReLU(inplace=True)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                conv_init(m)
            elif isinstance(m, nn.BatchNorm2d):
                bn_init(m, 1)
        bn_init(self.bn, 1e-6)


    def forward(self, x):
        y = None
        if self.adaptive:
            A = self.PA
        else:
            A = self.A.cuda(x.get_device())

        # A_subs = self.get_all_1_hop_subgraphs(A[0])
        # for i, A_sub in enumerate(A_subs):
        #     z = self.convs_subGraph[i](x, A_sub, self.alpha)
        #     y = z + y if y is not None else z

        for i in range(self.num_subset):
            z = self.convs[i](x, A[i], self.alpha)
            y = z + y if y is not None else z

        y = self.bn(y)
        y += self.down(x)
        y = self.relu(y)

        return y


class TCN_GCN_unit(nn.Module):
    def __init__(self, in_channels, out_channels, A, window_size=64, stride=1, residual=True, adaptive=False, kernel_size=5,
                 dilations=[1, 2], **kwargs):
        super(TCN_GCN_unit, self).__init__()
        self.embed_channels_out = out_channels
        self.gcn1 = unit_gcn(in_channels, out_channels, A, adaptive=adaptive, **kwargs)

        # unfold_window = 2
        # self.unfold = UnfoldTemporalWindows(window_size=unfold_window, window_stride=unfold_window, window_dilation=1)
        # self.tatt1 = AxialAttention_dynamic(in_channel=out_channels, out_channel=out_channels, kernel_size=window_size // unfold_window,  stride=1, spatial_dim=False)
        self.tatt1 = unit_att(out_channels, out_channels, window_size, [1, 2, 4, 8, 16], 1, False)

        self.relu = nn.ReLU(inplace=True)
        if not residual:
            self.residual = lambda x: 0

        elif (in_channels == out_channels) and (stride == 1):
            self.residual = lambda x: x

        else:
            self.residual = nn.Sequential(
                nn.Conv2d(
                    in_channels,
                    out_channels,
                    kernel_size=1,
                    stride=(1, 1)),
                nn.BatchNorm2d(out_channels),
            )
            # self.residual = unit_tcn(in_channels, out_channels, kernel_size=1, stride=stride)

    def forward(self, x):
        N, _, T, V = x.shape
        y = self.gcn1(x)
        # y = self.unfold(y)
        y = self.tatt1(y)
        y = y + self.residual(x)
        y = self.relu(y)
        return y

class TCN_GCN_unit2(nn.Module):
    def __init__(self, in_channels, out_channels, A, window_size=64, stride=1, residual=True, adaptive=False, kernel_size=5,
                 dilations=[1, 2], **kwargs):
        super(TCN_GCN_unit2, self).__init__()
        self.embed_channels_out = out_channels
        self.gcn1 = unit_gcn(in_channels, out_channels, A, adaptive=adaptive, **kwargs)
        self.gcn2 = unit_gcn(in_channels, out_channels, A, adaptive=adaptive, **kwargs)

        # unfold_window = 2
        # self.unfold = UnfoldTemporalWindows(window_size=unfold_window, window_stride=unfold_window, window_dilation=1)
        # self.tatt1 = AxialAttention_dynamic(in_channel=out_channels, out_channel=out_channels, kernel_size=window_size // unfold_window,  stride=1, spatial_dim=False)
        self.tatt1 = unit_att(out_channels, out_channels, window_size, [1, 2, 4, 8, 16], 1, False)
        self.tatt2 = unit_att(in_channels, out_channels * 2, window_size, [1, 2, 4, 8, 16], 1, False)

        self.relu = nn.ReLU(inplace=True)
        if not residual:
            self.residual = lambda x: 0

        elif (in_channels == out_channels) and (stride == 1):
            self.residual = lambda x: x

        else:
            self.residual = nn.Sequential(
                nn.Conv2d(
                    in_channels,
                    out_channels,
                    kernel_size=1,
                    stride=(1, 1)),
                nn.BatchNorm2d(out_channels),
            )
            # self.residual = unit_tcn(in_channels, out_channels, kernel_size=1, stride=stride)

    def forward(self, x):
        N, _, T, V = x.shape
        # y = self.unfold(y)
        y = self.tatt2(x)
        y = self.gcn2(y)

        y = y + self.residual(x)
        y = self.relu(y)
        return y

class Model(nn.Module):
    def __init__(self, num_class=60, num_point=25, num_person=2, window_size=64, graph=None, graph_args=dict(),
                 in_channels=3,
                 drop_out=0, adaptive=True):
        super(Model, self).__init__()

        if graph is None:
            raise ValueError()
        else:
            Graph = import_class(graph)
            self.graph = Graph(**graph_args)

        A = self.graph.A  # 3,25,25  # 原始
        A_ori = self.graph.A
        # A = np.ones_like((3, num_point, num_point))  # 全1
        # A = np.ones_like(A_ori)  # 全1
        # A = np.ones_like(self.graph.A) - np.eye(num_point)  # 全1，无环
        # A = np.ones_like(self.graph.A) + A_ori  # 全1 + 原始


        self.num_class = num_class
        self.num_point = num_point

        base_channel = 192

        self.data_bn = nn.BatchNorm1d(num_person * 3 * num_point)
        # self.data_bn = nn.BatchNorm1d(num_person * base_channel * num_point)

        self.sampled_tensor = nn.Parameter(torch.randn(1, window_size))  # 对数先验
        self.to_joint_embedding = nn.Linear(in_channels, base_channel)
        self.pos_embedding = nn.Parameter(torch.randn(1, self.num_point, base_channel))

        self.bias_emb = nn.Parameter(torch.randn(1, self.num_point, base_channel))

        # if len(A_ori.shape) <= 2:
        #     A_emb = torch.tensor(A_ori, dtype=torch.float32)
        # else:
        #     A_emb = torch.ones((num_point, num_point))

        # self.degree_emb = degree_centrality(A_emb)
        # self.betweenness_emb = betweenness_centrality(A_emb)
        # self.importance_emb = self.degree_emb + self.betweenness_emb
        # self.emb_linear = nn.Linear(self.degree_emb.shape[1], base_channel)


        # self.l1 = TCN_GCN_unit(base_channel, base_channel, A, window_size=window_size, adaptive=adaptive)
        # self.l2 = TCN_GCN_unit(base_channel, base_channel, A, window_size=window_size, adaptive=adaptive, loop_times=0)
        # self.l3 = TCN_GCN_unit(base_channel, base_channel, A, window_size=window_size, adaptive=adaptive, loop_times=0)
        # self.l4 = TCN_GCN_unit(base_channel, base_channel * 2, A, window_size=window_size, stride=2, adaptive=adaptive, loop_times=0)
        # self.l5 = TCN_GCN_unit(base_channel * 2, base_channel * 2, A, window_size=window_size, adaptive=adaptive, loop_times=0)
        # self.l6 = TCN_GCN_unit(base_channel * 2, base_channel * 2, A, window_size=window_size, adaptive=adaptive, loop_times=0)
        # self.l7 = TCN_GCN_unit(base_channel * 2, base_channel * 4, A, window_size=window_size, stride=2, adaptive=adaptive, loop_times=0)
        # self.l8 = TCN_GCN_unit(base_channel * 4, base_channel * 4, A, window_size=window_size, adaptive=adaptive, loop_times=0)
        # self.l9 = TCN_GCN_unit(base_channel * 4, base_channel * 4, A, window_size=window_size, adaptive=adaptive, loop_times=0)

        # ntu
        self.stem = nn.Sequential(
            nn.Conv2d(in_channels, base_channel, kernel_size=(4, 1), stride=(4, 1)),
            LayerNorm(base_channel, eps=1e-6, data_format="channels_first")
        )

        # # ucla
        # self.stem = nn.Sequential(
        #     nn.Conv2d(in_channels, base_channel, kernel_size=(1, 1), stride=(1, 1)),
        #     LayerNorm(base_channel, eps=1e-6, data_format="channels_first")
        # )

        # 动作生成
        self.pointConv = nn.Sequential(
            nn.Conv2d(base_channel, in_channels, kernel_size=(1, 1), stride=(1, 1)),
            # LayerNorm(base_channel, eps=1e-6, data_format="channels_first")
        )


        self.l1 = TCN_GCN_unit(base_channel, base_channel * 2, A, window_size=window_size, adaptive=adaptive, loop_times=0)
        self.l2 = TCN_GCN_unit2(base_channel * 2, base_channel, A, window_size=window_size, adaptive=adaptive, loop_times=0)

        self.gelu = nn.GELU()
        self.relu = nn.ReLU()
        self.fc = nn.Linear(base_channel, num_class)
        self.aux_fc = nn.Conv2d(base_channel, 1, 1, 1)
        nn.init.normal_(self.fc.weight, 0, math.sqrt(2. / num_class))
        bn_init(self.data_bn, 1)
        if drop_out:
            self.drop_out = nn.Dropout(drop_out)
        else:
            self.drop_out = lambda x: x

        self.norm = nn.LayerNorm(base_channel, eps=1e-6)


    def forward(self, x):
        if len(x.shape) == 3:
            N, T, VC = x.shape
            x = x.view(N, T, self.num_point, -1).permute(0, 3, 1, 2).contiguous().unsqueeze(-1)
        N, C, T, V, M = x.size()

        # 位置编码
        # x = rearrange(x, 'n c t v m -> (n m t) v c', m=M, v=V).contiguous()

        # x = self.to_joint_embedding(x)
        # x += self.pos_embedding[:, :self.num_point]

        # 节点的中心性编码
        # x += self.emb_linear(self.degree_emb.cuda(x.get_device())).unsqueeze(0)  # 度中心性
        # x += self.emb_linear(self.betweenness_emb.cuda(x.get_device())).unsqueeze(0)  # 介数中心性
        # x += self.emb_linear(self.importance_emb.cuda(x.get_device())).unsqueeze(0)  # 混合重要性

        # x = rearrange(x, '(n m t) v c -> n (m v c) t', m=M, t=T).contiguous()
        x = rearrange(x, 'n c t v m -> n (m v c) t', m=M, t=T).contiguous()

        x = self.data_bn(x)
        x = rearrange(x, 'n (m v c) t -> (n m) c t v', m=M, v=V).contiguous()
        x = self.stem(x)



        # 先验知识
        # mean = torch.mean(x)
        # std = torch.std(x)
        # x = rearrange(x, 'n c t v m -> (n m) t (c v)')

        # s = rearrange(self.sampled_tensor, 'n x -> n 1 x 1').contiguous()

        # s = s * std + mean
        # Visualzation.plot_distribution(x, title="before")

        # x = s + x

        # Visualzation.plot_distribution(x, 'after')


        # 表示学习
        x = self.l1(x)
        x9 = self.l2(x)
        # x1 = self.l1(x)  # (2, 64, T, V)
        # x2 = self.l2(x1)  # (2, 64, T, V)
        # x3 = self.l3(x2)  # (2, 64, T, V)
        # x4 = self.l4(x3)  # (2, 128, T, V)
        # x5 = self.l5(x4)  # (2, 128, T, V)
        # x6 = self.l6(x5)  # (2, 128, T, V)
        # x7 = self.l7(x6)  # (2, 256, T, V)
        # x8 = self.l8(x7)  # (2, 256, T, V)
        # x9 = self.l9(x8)
        # x9 = self.l9(x8) + self.lange_range_res1(x1) + self.lange_range_res2(x4) + x7

        # 动作生成
        # points = self.pointConv(x)


        # N*M,C,T,V
        c_new = x9.size(1)

        x_out = x9.view(N, M, c_new, -1)
        x_out = x_out.mean(3).mean(1)
        x_out = self.norm(x_out)
        x_out = self.drop_out(x_out)

        return self.fc(x_out)


if __name__ == '__main__':
    print(os.path.abspath(__file__))
    net = Model(graph="graph.ntu_rgb_d.Graph",
                # graph_args={"labeling_mode": '1-hot-subgraph'},
                # graph_args={"labeling_mode": 'spatial-and-centrality'},
                graph_args={"labeling_mode": 'spatial'}).cuda()
    ske = torch.rand([1, 3, 256, 25, 2]).cuda()

    result = net(ske)

    # # 查看网络结构图
    # with SummaryWriter(comment='DSTA-Net') as w:
    #     w.add_graph(net, (ske,))

    # 模型参数量&推理计算量
    flops, params = profile(net, inputs=(ske,))
    print('Flops: ', flops, 'Params: ', params)

    print('FLOPs&Param:' + 'GFLOPs: %.2f G, Param: %.2f M' % (flops / 1000000000.0, params / 1000000.0))
    print(result.shape)
