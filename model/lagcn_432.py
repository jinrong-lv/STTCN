import math
from math import comb

import numpy as np
import torch
import torch.nn as nn
from thop import profile
from torch.autograd import Variable

from einops import rearrange, repeat

combinatorial_complex_lists = {
    # 头部
    'head': [(3, 2)],
    # 颈部
    'neck': [(2, 20)],
    # 腹部
    'abdomen': [(8, 20, 4, 1, 16, 0, 12)],
    # 全身
    'body': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 1, 12, 13, 14, 1, 3, 2, 1)],
    # 四肢
    'arms_and_legs': [(24, 23, 11, 10, 9, 8, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 12, 13, 14, 15)],
    # 左右手
    'left_and_right_hands': [(24, 23, 11, 10, 6, 7, 21, 22)],
    # 左右脚
    'left_and_right_feets': [(19, 18, 15, 14)],
    # 左右手臂
    'left_and_right_arms': [(10, 9, 8, 4, 5, 6)],
    # 左右腿
    'left_and_right_thighs': [(17, 16, 13, 12)],
    # 左半边身体
    'left_body': [(24, 23, 11, 10, 9, 8, 20, 1, 19, 20, 17, 16, 0)],
    # 右半边身体
    'right_body': [(20, 4, 5, 6, 7, 21, 22, 1, 0, 12, 13, 14, 15)],
    # 上躯干
    'upper_torso': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22)],
    # 左上臂
    'left_upper_arm': [(24, 23, 11, 10, 9, 8, 20)],
    # 右上臂
    'right_upper_arm': [(20, 4, 5, 6, 7, 21, 22)],
    # 左前臂
    'left_forearm': [(24, 23, 11, 10, 9)],
    # 右前臂
    'right_forearm': [(5, 6, 7, 21, 22)],
    # 左手
    'left_hand': [(24, 23, 11, 10)],
    # 右手
    'right_hand': [(6, 7, 21, 22)],
    # 下躯干
    'lower_torso': [(19, 18, 17, 16, 0, 12, 13, 14, 15)],
    # 左大腿
    'left_thigh': [(18, 17, 0)],
    # 右大腿
    'right_thigh': [(13, 12, 0)],
    # 左小腿
    'left_calf': [(19, 18, 17)],
    # 右小腿
    'right_calf': [(15, 14, 13)],
    # 左脚
    'left_foot': [(19, 18)],
    # 右脚
    'right_foot': [(15, 14)]
}


# 单纯复形
# 计算两点之间的距离的函数
def calculate_distances(points1, points2):
    return torch.sqrt(torch.abs(torch.sum((points1 - points2) ** 2, dim=0)))


# Heron公式计算三角形面积的函数
def calculate_triangle_areas(a, b, c):
    s = (a + b + c) / 2
    area = torch.sqrt(torch.abs(s * (s - a) * (s - b) * (s - c)))
    return area


# 归一化面积张量
def normalize_area(tensor):
    min_val = torch.min(tensor)
    max_val = torch.max(tensor)
    normalized_tensor = (tensor - min_val) / (max_val - min_val)
    return normalized_tensor


# 主函数，计算基于关节点构建的三角形面积和周长的总和，以及坐标的均值
def calculate_features(skeleton_data):
    # 初始化特征张量
    global joint0
    feature_tensor = torch.zeros((3, *skeleton_data.size()[1:]))

    # 定义关节点的选择方案
    # 这里需要根据实际的骨架数据结构来确定关节点的索引
    joint_selection_schemes = {
        # 头部
        'head': [(23, 3, 2)],
        # 颈部
        'neck': [(21, 2, 20)],
        # 腹部
        # 'abdomen': [(8, 20, 4, 1, 16, 0, 12)],
        'abdomen': [(8, 20, 1), (8, 1, 16), (1, 16, 0), (4, 20, 1), (4, 1, 12), (1, 0, 12)],
        # 全身
        # 'body': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 1, 12, 13, 14, 1, 3, 2, 1)],
        'body': [(23, 3, 19), (21, 3, 15)],
        # 四肢
        # 'arms_and_legs': [(24, 23, 11, 10, 9, 8, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 12, 13, 14, 15)],
        'arms_and_legs': [(23, 16, 19), (21, 19, 15)],
        # 左右手
        # 'left_and_right_hands': [(24, 23, 11, 10, 6, 7, 21, 22)],
        'left_and_right_hands': [(23, 21, 20)],
        # 左右脚
        # 'left_and_right_feets': [(19, 18, 15, 14)],
        'left_and_right_feets': [(19, 0, 15)],
        # 左右手臂
        # 'left_and_right_arms': [(10, 9, 8, 4, 5, 6)],
        'left_and_right_arms': [(10, 20, 6)],
        # 左右腿
        # 'left_and_right_thighs': [(17, 16, 13, 12)],
        'left_and_right_thighs': [(17, 0, 13)],
        # 左半边身体
        # 'left_body': [(24, 23, 11, 10, 9, 8, 20, 1, 19, 20, 17, 16, 0)],
        'left_body': [(23, 1, 19)],
        # 右半边身体
        # 'right_body': [(20, 4, 5, 6, 7, 21, 22, 1, 0, 12, 13, 14, 15)],
        'right_body': [(21, 1, 15)],
        # 上躯干
        # 'upper_torso': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22)],
        'upper_torso': [(23, 0, 21)],
        # 左上臂
        # 'left_upper_arm': [(24, 23, 11, 10, 9, 8, 20)],
        'left_upper_arm': [(23, 20, 0)],
        # 右上臂
        # 'right_upper_arm': [(20, 4, 5, 6, 7, 21, 22)],
        'right_upper_arm': [(21, 20, 0)],
        # 左前臂
        # 'left_forearm': [(24, 23, 11, 10, 9)],
        'left_forearm': [(23, 20, 1)],
        # 右前臂
        # 'right_forearm': [(5, 6, 7, 21, 22)],
        'right_forearm': [(21, 20, 1)],
        # 左手
        # 'left_hand': [(24, 23, 11, 10)],
        'left_hand': [(24, 23, 11)],
        # 右手
        # 'right_hand': [(6, 7, 21, 22)],
        'right_hand': [(7, 21, 22)],
        # 下躯干
        # 'lower_torso': [(19, 18, 17, 16, 0, 12, 13, 14, 15)],
        'lower_torso': [(19, 0, 15)],
        # 左大腿
        # 'left_thigh': [(18, 17, 0)],
        'left_thigh': [(18, 17, 0)],
        # 右大腿
        # 'right_thigh': [(13, 12, 0)],
        'right_thigh': [(13, 12, 0)],
        # 左小腿
        # 'left_calf': [(19, 18, 17)],
        'left_calf': [(19, 18, 17)],
        # 右小腿
        # 'right_calf': [(15, 14, 13)],
        'right_calf': [(15, 14, 13)],
        # 左脚
        # 'left_foot': [(19, 18)],
        'left_foot': [(19, 18, 17)],
        # 右脚
        # 'right_foot': [(15, 14)]
        'right_foot': [(15, 14, 13)]
    }

    # 对于每个关节点方案，计算所有帧和所有人的三角形面积和周长
    for joint_part, schemes in joint_selection_schemes.items():
        # 初始化存储面积和周长的变量
        total_area = 0
        total_perimeter = 0
        # 存储构成三角形的所有关节点坐标
        all_points = []

        for (joint0, joint1, joint2) in schemes:
            # 提取关节点坐标
            point0 = skeleton_data[:, :, joint0, :]
            point1 = skeleton_data[:, :, joint1, :]
            point2 = skeleton_data[:, :, joint2, :]
            all_points += [point0, point1, point2]

            # 计算三边长度
            a = calculate_distances(point0, point1)
            b = calculate_distances(point1, point2)
            c = calculate_distances(point0, point2)

            # 计算面积和周长
            total_area += calculate_triangle_areas(a, b, c)
            total_perimeter += a + b + c

        # 计算坐标的均值
        mean_points = torch.mean(torch.stack(all_points), dim=(0, 1))

        # 将计算结果存储在特征张量中
        feature_tensor[0, :, joint0, :] = total_area
        feature_tensor[1, :, joint0, :] = total_perimeter
        feature_tensor[2, :, joint0, :] = mean_points

    return feature_tensor


def import_class(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def conv_branch_init(conv, branches):
    weight = conv.weight
    n = weight.size(0)
    k1 = weight.size(1)
    k2 = weight.size(2)
    nn.init.normal_(weight, 0, math.sqrt(2. / (n * k1 * k2 * branches)))
    nn.init.constant_(conv.bias, 0)


def conv_init(conv):
    if conv.weight is not None:
        nn.init.kaiming_normal_(conv.weight, mode='fan_out')
    if conv.bias is not None:
        nn.init.constant_(conv.bias, 0)


def bn_init(bn, scale):
    nn.init.constant_(bn.weight, scale)
    nn.init.constant_(bn.bias, 0)


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        if hasattr(m, 'weight'):
            nn.init.kaiming_normal_(m.weight, mode='fan_out')
        if hasattr(m, 'bias') and m.bias is not None and isinstance(m.bias, torch.Tensor):
            nn.init.constant_(m.bias, 0)
    elif classname.find('BatchNorm') != -1:
        if hasattr(m, 'weight') and m.weight is not None:
            m.weight.data.normal_(1.0, 0.02)
        if hasattr(m, 'bias') and m.bias is not None:
            m.bias.data.fill_(0)


def calculate_node_counts(combinatorial_complex_lists):
    # 计算每个组合复合体的不同节点个数
    node_counts = [len(set(complex[0])) for complex in combinatorial_complex_lists.values()]
    # 将列表转换为[E, C]维度的张量
    node_counts_tensor = torch.tensor(node_counts).view(-1, 1)
    return node_counts_tensor


def calculate_edge_counts(combinatorial_complex_lists):
    # 计算每个组合复合体的不同节点个数
    node_counts = [len(set(complex[0])) - 1 for complex in combinatorial_complex_lists.values()]
    # 将列表转换为[E, C]维度的张量
    node_counts_tensor = torch.tensor(node_counts).view(-1, 1)
    return node_counts_tensor


# class LayerNorm(nn.Module):
#     r""" LayerNorm that supports two data formats: channels_last (default) or channels_first.
#     The ordering of the dimensions in the inputs. channels_last corresponds to inputs with
#     shape (batch_size, height, width, channels) while channels_first corresponds to inputs
#     with shape (batch_size, channels, height, width).
#     """
#
#     def __init__(self, normalized_shape, eps=1e-6, data_format="channels_last"):
#         super().__init__()
#         self.weight = nn.Parameter(torch.ones(normalized_shape))
#         self.bias = nn.Parameter(torch.zeros(normalized_shape))
#         self.eps = eps
#         self.data_format = data_format
#         if self.data_format not in ["channels_last", "channels_first"]:
#             raise NotImplementedError
#         self.normalized_shape = (normalized_shape,)
#
#     def forward(self, x):
#         if self.data_format == "channels_last":
#             return F.layer_norm(x, self.normalized_shape, self.weight, self.bias, self.eps)
#         elif self.data_format == "channels_first":
#             u = x.mean(1, keepdim=True)
#             s = (x - u).pow(2).mean(1, keepdim=True)
#             x = (x - u) / torch.sqrt(s + self.eps)
#             x = self.weight[:, None, None] * x + self.bias[:, None, None]
#             return x


class TemporalConv(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, dilation=1):
        super(TemporalConv, self).__init__()
        pad = (kernel_size + (kernel_size - 1) * (dilation - 1) - 1) // 2
        self.conv = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size=(kernel_size, 1),
            padding=(pad, 0),
            stride=(stride, 1),
            dilation=(dilation, 1))

        self.bn = nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        return x


class MultiScale_TemporalConv(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=3,
                 stride=1,
                 dilations=[1, 2, 3, 4],
                 residual=True,
                 residual_kernel_size=1):

        super().__init__()
        assert out_channels % (len(dilations) + 2) == 0, '# out channels should be multiples of # branches'

        # Multiple branches of temporal convolution
        self.num_branches = len(dilations) + 2
        branch_channels = out_channels // self.num_branches
        if type(kernel_size) == list:
            assert len(kernel_size) == len(dilations)
        else:
            kernel_size = [kernel_size] * len(dilations)
        # Temporal Convolution branches
        self.branches = nn.ModuleList([
            nn.Sequential(
                nn.Conv2d(
                    in_channels,
                    branch_channels,
                    kernel_size=1,
                    padding=0),
                nn.BatchNorm2d(branch_channels),
                nn.ReLU(inplace=True),
                TemporalConv(
                    branch_channels,
                    branch_channels,
                    kernel_size=ks,
                    stride=stride,
                    dilation=dilation),
            )
            for ks, dilation in zip(kernel_size, dilations)
        ])

        # Additional Max & 1x1 branch
        self.branches.append(nn.Sequential(
            nn.Conv2d(in_channels, branch_channels, kernel_size=1, padding=0),
            nn.BatchNorm2d(branch_channels),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=(3, 1), stride=(stride, 1), padding=(1, 0)),
            nn.BatchNorm2d(branch_channels)  # 为什么还要加bn
        ))

        self.branches.append(nn.Sequential(
            nn.Conv2d(in_channels, branch_channels, kernel_size=1, padding=0, stride=(stride, 1)),
            nn.BatchNorm2d(branch_channels)
        ))

        # Residual connection
        if not residual:
            self.residual = lambda x: 0
        elif (in_channels == out_channels) and (stride == 1):
            self.residual = lambda x: x
        else:
            self.residual = TemporalConv(in_channels, out_channels, kernel_size=residual_kernel_size, stride=stride)

        # initialize
        self.apply(weights_init)

    def forward(self, x):
        # Input dim: (N,C,T,V)
        res = self.residual(x)
        branch_outs = []
        for tempconv in self.branches:
            out = tempconv(x)
            branch_outs.append(out)

        out = torch.cat(branch_outs, dim=1)
        out += res
        return out


class CTRGC(nn.Module):
    def __init__(self, in_channels, out_channels, rel_reduction=8, mid_reduction=1, loop_times=0, fuse_alpha=0.15):
        super(CTRGC, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        if in_channels == 3 or in_channels == 9:
            self.rel_channels = 8
            self.mid_channels = 16
        else:
            self.rel_channels = in_channels // rel_reduction
            self.mid_channels = in_channels // mid_reduction
        self.conv1 = nn.Conv2d(self.in_channels, self.rel_channels, kernel_size=1)
        self.conv2 = nn.Conv2d(self.in_channels, self.rel_channels, kernel_size=1)
        self.conv3 = nn.Conv2d(self.in_channels, self.out_channels, kernel_size=1)
        self.conv4 = nn.Conv2d(self.rel_channels, self.out_channels, kernel_size=1)
        self.tanh = nn.Tanh()
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                conv_init(m)
            elif isinstance(m, nn.BatchNorm2d):
                bn_init(m, 1)

        self.loop_times = loop_times
        self.fuse_alpha = fuse_alpha
        self.edge_features = calculate_node_counts(combinatorial_complex_lists)

    # def complex_feature_constructor(self, x, combinatorial_complex_lists):
    #     # x3的维度为[n, c, t, v]，其中v表示人体关键点的数量
    #     N, C, T, V = x.shape
    #     # 创建一个新的张量来存储重构后的信息
    #     x_reconstructed = torch.zeros_like(x)
    #
    #     # 遍历字典中的每个人体区域
    #     for region, indices_list in combinatorial_complex_lists.items():
    #         for indices in indices_list:
    #             # 根据区域内的关节索引计算平均值
    #             region_values = x[:, :, :, list(indices)].mean(dim=-1)
    #             # 将计算得到的平均值赋值给对应的关节索引位置
    #             for idx in indices:
    #                 x_reconstructed[:, :, :, idx] = region_values
    #
    #     return x_reconstructed

    def complex_feature_constructor(self, x, combinatorial_complex_lists):
        # x的维度为[N, C, T, V]，其中V表示人体关键点的数量
        N, C, T, V = x.shape
        # 创建一个新的张量来存储重构后的信息
        x_reconstructed = torch.zeros_like(x)

        # 遍历字典中的每个人体区域
        for region, indices_list in combinatorial_complex_lists.items():
            # 获取所有区域内关节索引的平均值
            indices_tensor = torch.tensor(list(indices_list), device=x.device).view(1, 1, 1, -1)
            region_values = x[:, :, :, indices_tensor].mean(dim=-1)
            # 使用广播机制赋值
            x_reconstructed[:, :, :, indices_tensor] = region_values.unsqueeze(-1)

        return x_reconstructed

    def k_hop(self, A):
        # A: N, C, V, V
        N, C, V, _ = A.shape
        # A0: 1, 1, V, V
        A0 = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0) * self.fuse_alpha

        A_power = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0)
        for i in range(1, self.loop_times + 1):
            A_power = torch.einsum('ncuv,ncvw->ncuw', A, A_power)
            A0 = A_power * (self.fuse_alpha * (1 - self.fuse_alpha) ** i) + A0

        return A0

    def k_hop_incidence(self, A):
        # A: N, C, E, V (关联矩阵，E是边的数量)
        N, C, E, V = A.shape
        # A0: 1, 1, V, V (初始化为单位矩阵，扩展到与A相同的批次和通道数)
        A0 = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0) * self.fuse_alpha

        # 计算关联矩阵的转置
        A_transpose = A.transpose(-1, -2)
        # 初始化A_power为单位矩阵
        A_power = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0)

        for i in range(1, self.loop_times + 1):
            # 使用关联矩阵和其转置的乘积来传递信息
            A_power = torch.einsum('ncuv,ncvw->ncuw', A, A_transpose @ A_power)
            # 更新A0，加入新的高阶邻接信息
            A0 = A_power * (self.fuse_alpha * (1 - self.fuse_alpha) ** i) + A0

        return A0

    def k_hop_edge_incidence(self, A):
        # A: N, C, V, E (关联矩阵，E是边的数量)
        N, C, V, E = A.shape
        # A0: 1, 1, V, V (初始化为单位矩阵，扩展到与A相同的批次和通道数)
        A0 = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0) * self.fuse_alpha

        # 初始化边的特征权重
        edge_weight = self.edge_features.to(A.device).unsqueeze(0).unsqueeze(0)

        # 初始化A_power为单位矩阵
        A_power = torch.eye(V, dtype=A.dtype).to(A.device).unsqueeze(0).unsqueeze(0)

        for i in range(1, self.loop_times + 1):
            # 计算关联矩阵的高阶幂
            A_power = torch.einsum('ncuv,ncvw->ncuw', A, A_power)
            # 融合边的特征
            A_power_weighted = A_power * edge_weight
            # 更新A0，加入新的高阶邻接信息和边的特征
            A0 = A_power_weighted * (self.fuse_alpha * (1 - self.fuse_alpha) ** i) + A0

        return A0

    def forward(self, x, A=None, alpha=1):
        # x1, x2, x3 = self.conv1(x).mean(-2), self.conv2(x).mean(-2), self.conv3(x)
        # x1 = self.tanh(x1.unsqueeze(-1) - x2.unsqueeze(-2))
        # x1 = self.conv4(x1) * alpha + (A.unsqueeze(0).unsqueeze(0) if A is not None else 0)  # N,C,V,V

        x3 = self.conv3(x)  # N, C, T, V
        x1 = A.unsqueeze(0).unsqueeze(0) if A is not None else 0  # N,C,V,V

        # import time
        # start_time = time.perf_counter()
        # x3_1 = self.complex_feature_constructor(x3, combinatorial_complex_lists)
        x3 = self.complex_feature_constructor(x3, combinatorial_complex_lists)
        # end_time = time.perf_counter()
        # print(f"运行时间：{end_time - start_time} 秒")

        # if self.loop_times != 0:
        #     fuse_A = self.k_hop(x1)
        #     out_k = torch.einsum('ncuv,nctv->nctu', fuse_A, x3)
        #     return out_k
        # else:
        #     x1 = torch.einsum('ncuv,nctv->nctu', x1, x3)
        #     return x1
        x1 = torch.einsum('ncuv,nctv->nctu', x1, x3)
        return x1


class unit_tcn(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=9, stride=1):
        super(unit_tcn, self).__init__()
        pad = int((kernel_size - 1) / 2)
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=(kernel_size, 1), padding=(pad, 0),
                              stride=(stride, 1))

        self.bn = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        conv_init(self.conv)
        bn_init(self.bn, 1)

    def forward(self, x):
        x = self.bn(self.conv(x))
        return x


class unit_gcn(nn.Module):
    def __init__(self, in_channels, out_channels, A, coff_embedding=4, adaptive=True, residual=True, **kwargs):
        super(unit_gcn, self).__init__()
        inter_channels = out_channels // coff_embedding
        self.inter_c = inter_channels
        self.out_c = out_channels
        self.in_c = in_channels
        self.adaptive = adaptive
        self.num_subset = A.shape[0]
        self.convs = nn.ModuleList()
        for i in range(self.num_subset):
            self.convs.append(CTRGC(in_channels, out_channels, **kwargs))

        if residual:
            if in_channels != out_channels:
                self.down = nn.Sequential(
                    nn.Conv2d(in_channels, out_channels, 1),
                    nn.BatchNorm2d(out_channels)
                )
            else:
                self.down = lambda x: x
        else:
            self.down = lambda x: 0
        if self.adaptive:
            self.PA = nn.Parameter(torch.from_numpy(A.astype(np.float32)))
        else:
            self.A = Variable(torch.from_numpy(A.astype(np.float32)), requires_grad=False)
        self.alpha = nn.Parameter(torch.zeros(1))
        self.bn = nn.BatchNorm2d(out_channels)
        self.soft = nn.Softmax(-2)
        self.relu = nn.ReLU(inplace=True)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                conv_init(m)
            elif isinstance(m, nn.BatchNorm2d):
                bn_init(m, 1)
        bn_init(self.bn, 1e-6)

    def forward(self, x):
        y = None
        if self.adaptive:
            A = self.PA
        else:
            A = self.A.cuda(x.get_device())
        for i in range(self.num_subset):
            z = self.convs[i](x, A[i], self.alpha)
            y = z + y if y is not None else z
        y = self.bn(y)
        y += self.down(x)
        y = self.relu(y)

        return y


class TCN_GCN_unit(nn.Module):
    def __init__(self, in_channels, out_channels, A, stride=1, residual=True, adaptive=True, kernel_size=5,
                 dilations=[1, 2], **kwargs):
        super(TCN_GCN_unit, self).__init__()
        self.gcn1 = unit_gcn(in_channels, out_channels, A, adaptive=adaptive, **kwargs)
        self.tcn1 = MultiScale_TemporalConv(out_channels, out_channels, kernel_size=kernel_size, stride=stride,
                                            dilations=dilations,
                                            residual=False)
        self.relu = nn.ReLU(inplace=True)
        if not residual:
            self.residual = lambda x: 0

        elif (in_channels == out_channels) and (stride == 1):
            self.residual = lambda x: x

        else:
            self.residual = unit_tcn(in_channels, out_channels, kernel_size=1, stride=stride)

    def forward(self, x):
        y = self.relu(self.tcn1(self.gcn1(x)) + self.residual(x))
        return y


class Model(nn.Module):
    def __init__(self, num_class=60, num_point=25, num_person=2, graph=None, graph_args=dict(), in_channels=3,
                 drop_out=0, adaptive=True):
        super(Model, self).__init__()

        if graph is None:
            raise ValueError()
        else:
            Graph = import_class(graph)
            self.graph = Graph(**graph_args)

        A = self.graph.A  # 3,25,25

        self.num_class = num_class
        self.num_point = num_point

        base_channel = 64

        self.data_bn = nn.BatchNorm1d(num_person * base_channel * num_point)
        self.to_joint_embedding = nn.Linear(in_channels, base_channel)
        self.pos_embedding = nn.Parameter(torch.randn(1, self.num_point, base_channel))

        # # 跑ucla时注释下面四行
        # self.stem = nn.Sequential(
        #     # nn.Conv2d(base_channel, base_channel, kernel_size=(4, 1), stride=(4, 1)),
        #     MultiScale_TemporalConv(base_channel, base_channel, kernel_size=4, stride=4, dilations=[1, 2]),
        #     LayerNorm(base_channel, eps=1e-6, data_format="channels_first")
        # )

        self.l1 = TCN_GCN_unit(base_channel, base_channel, A, adaptive=adaptive)
        self.l2 = TCN_GCN_unit(base_channel, base_channel, A, adaptive=adaptive, loop_times=0)
        self.l3 = TCN_GCN_unit(base_channel, base_channel, A, adaptive=adaptive, loop_times=0)
        self.l4 = TCN_GCN_unit(base_channel, base_channel * 2, A, stride=2, adaptive=adaptive, loop_times=0)
        self.l5 = TCN_GCN_unit(base_channel * 2, base_channel * 2, A, adaptive=adaptive, loop_times=0)
        self.l6 = TCN_GCN_unit(base_channel * 2, base_channel * 2, A, adaptive=adaptive, loop_times=0)
        self.l7 = TCN_GCN_unit(base_channel * 2, base_channel * 4, A, stride=2, adaptive=adaptive, loop_times=0)
        self.l8 = TCN_GCN_unit(base_channel * 4, base_channel * 4, A, adaptive=adaptive, loop_times=0)
        self.l9 = TCN_GCN_unit(base_channel * 4, base_channel * 4, A, adaptive=adaptive, loop_times=0)

        # Retrospect Model
        self.first_tram = nn.Sequential(
            nn.AvgPool2d((4, 1)),
            nn.Conv2d(64, 64 * 4, 1),
            nn.BatchNorm2d(64 * 4),
            nn.ReLU()
        )
        self.second_tram = nn.Sequential(
            nn.AvgPool2d((2, 1)),
            nn.Conv2d(64 * 2, 64 * 4, 1),
            nn.BatchNorm2d(64 * 4),
            nn.ReLU()
        )

        self.fc = nn.Linear(base_channel * 4, num_class)
        # self.aux_fc = nn.Conv2d(base_channel * 4, 1, 1, 1)
        nn.init.normal_(self.fc.weight, 0, math.sqrt(2. / num_class))
        bn_init(self.data_bn, 1)
        if drop_out:
            self.drop_out = nn.Dropout(drop_out)
        else:
            self.drop_out = lambda x: x

    def forward(self, x):
        if len(x.shape) == 3:
            N, T, VC = x.shape
            x = x.view(N, T, self.num_point, -1).permute(0, 3, 1, 2).contiguous().unsqueeze(-1)
        N, C, T, V, M = x.size()

        x = rearrange(x, 'n c t v m -> (n m t) v c', m=M, v=V).contiguous()

        x = self.to_joint_embedding(x)
        x += self.pos_embedding[:, :self.num_point]
        x = rearrange(x, '(n m t) v c -> n (m v c) t', m=M, t=T).contiguous()

        x = self.data_bn(x)
        x = rearrange(x, 'n (m v c) t -> (n m) c t v', m=M, v=V).contiguous()

        # x = self.stem(x)

        x = self.l1(x)
        x = self.l2(x)
        x = self.l3(x)
        x2 = x
        x = self.l4(x)
        x = self.l5(x)
        x = self.l6(x)
        x3 = x
        x = self.l7(x)
        x = self.l8(x)
        x = self.l9(x)

        x2 = self.first_tram(x2)  # x2 (N*M,64,75,25)
        x3 = self.second_tram(x3)  # x3 (N*M,128,75,25)
        x = x + x2 + x3

        # N*M,C,T,V
        c_new = x.size(1)

        # # aux branch
        # # N * M, C, T, V -> N * M, C, V
        # aux_x = x.mean(2)
        # # N * M, C, V -> N * M, C, n_cls, V
        # aux_x = torch.einsum('nmv,cvu->nmcu', aux_x, self.examplar)
        # #  N * M, C, n_cls, V ->  N * M, n_cls, V
        # aux_x = self.aux_fc(aux_x)
        # aux_x = aux_x.squeeze(1)
        # # N * M, n_cls, V -> N * M, n_cls
        # aux_x = aux_x.mean(2)
        #
        # aux_x = aux_x.reshape(N, M, self.num_class)
        # aux_x = aux_x.mean(dim=1)

        x = x.view(N, M, c_new, -1)
        x = x.mean(3).mean(1)
        x = self.drop_out(x)

        return self.fc(x)


if __name__ == '__main__':
    # print(os.path.abspath(__file__))
    net = Model(graph="graph.ntu_rgb_d.Graph",
                # graph_args={"labeling_mode": '1-hot-subgraph'},
                # graph_args={"labeling_mode": 'spatial-and-centrality'},
                graph_args={"labeling_mode": 'combinatorial_complex'}).cuda()
    ske = torch.rand([1, 3, 64, 25, 2]).cuda()

    result = net(ske)

    # # 查看网络结构图
    # with SummaryWriter(comment='DSTA-Net') as w:
    #     w.add_graph(net, (ske,))

    # 模型参数量&推理计算量
    flops, params = profile(net, inputs=(ske,))
    print('Flops: ', flops, 'Params: ', params)

    print('FLOPs&Param:' + 'GFLOPs: %.2f G, Param: %.2f M' % (flops / 1000000000.0, params / 1000000.0))
    print(result.shape)
