import torch
import numpy as np

# 定义一个非负矩阵的类
class NonnegativeMatrix:
    def __init__(self, matrix):
        # 检查矩阵是否是非负的
        assert torch.all(matrix >= 0), "The matrix must be nonnegative."
        # 检查矩阵是否是方阵
        assert matrix.shape[0] == matrix.shape[1], "The matrix must be square."
        # 保存矩阵
        self.matrix = matrix
        # 计算谱半径和正特征向量
        self.perron_root, self.perron_vector = self.perron_frobenius()

    # 定义一个计算谱半径和正特征向量的方法
    def perron_frobenius(self):
        # 使用幂法求解最大特征值和特征向量
        # 初始化一个随机向量
        x = torch.rand(self.matrix.shape[0]).cuda()
        # 设置一个容差
        tol = 1e-6
        # 设置一个最大迭代次数
        max_iter = 1000
        # 初始化一个误差
        error = np.inf
        # 初始化一个迭代次数
        iter = 0
        # 循环直到误差小于容差或达到最大迭代次数
        while error > tol and iter < max_iter:
            # 计算矩阵乘向量
            y = self.matrix @ x
            # 计算向量的范数
            norm = torch.norm(y)
            # 归一化向量
            y = y / norm
            # 计算误差
            error = torch.norm(x - y)
            # 更新向量
            x = y
            # 更新迭代次数
            iter += 1
        # 返回谱半径和正特征向量
        return norm, x

# 定义一个分类头部的类
class PerronFrobeniusClassifier(torch.nn.Module):
    def __init__(self, input_size, num_classes):
        # 调用父类的构造函数
        super(PerronFrobeniusClassifier, self).__init__()
        # 保存输入大小和类别数
        self.input_size = input_size
        self.num_classes = num_classes
        # 初始化一个权重矩阵，它是一个非负矩阵
        self.weight = torch.nn.Parameter(torch.rand(input_size, input_size))
        # 初始化一个偏置向量，它是一个非负向量
        self.bias = torch.nn.Parameter(torch.rand(input_size))
        # 确保权重矩阵和偏置向量是非负的
        self.weight.data.clamp_(min=0)
        self.bias.data.clamp_(min=0)

    # 定义一个前向传播的方法
    def forward(self, input):
        # 检查输入张量的维度
        assert input.ndim == 2, "The input tensor must have 2 dimensions."
        # 获取输入张量的形状
        batch_size, input_size = input.shape
        # 检查输入大小是否匹配
        assert input_size == self.input_size, "The input size must match the weight matrix size."
        # 初始化一个输出张量
        output = torch.zeros(batch_size, self.num_classes).to(input.get_device())
        # 遍历每个批次
        for b in range(batch_size):
            # 获取输入向量
            x = input[b]
            # 将输入向量转换成矩阵
            x = torch.diag(x)
            # 计算加权和
            z = x @ self.weight + self.bias
            # 创建一个非负矩阵的对象
            nn_matrix = NonnegativeMatrix(z)
            # 获取谱半径和正特征向量
            perron_root, perron_vector = nn_matrix.perron_root, nn_matrix.perron_vector
            # 将谱半径和正特征向量的平均值作为输出张量的元素
            output[b] = (perron_root + torch.mean(perron_vector)) / 2
        # 返回输出张量
        return output

# 定义一个激活函数
def softmax(input):
    # 计算指数
    exp = torch.exp(input)
    # 计算和
    sum = torch.sum(exp, dim=1, keepdim=True)
    # 计算概率
    prob = exp / sum
    # 返回概率
    return prob

# # 测试分类头部
# # 创建一个随机输入张量
# input = torch.rand(2, 96)
# # 设置一个类别数
# num_classes = 60
# # 创建一个分类头部的对象
# classifier = PerronFrobeniusClassifier(96, 60)
# # 调用分类头部的方法
# output = classifier(input)
# # 调用激活函数
# prob = softmax(output)
# # 打印输出张量的形状和值
# print(output.shape)
# print(output)
# # 打印概率张量的形状和值
# print(prob.shape)
# print(prob)
