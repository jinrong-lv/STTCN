import torch
import numpy as np

# 定义一个非负矩阵的类
class NonnegativeMatrix:
    def __init__(self, matrix):
        # 检查矩阵是否是非负的
        assert torch.all(matrix >= 0), "The matrix must be nonnegative."
        # 检查矩阵是否是方阵
        assert matrix.shape[0] == matrix.shape[1], "The matrix must be square."
        # 保存矩阵
        self.matrix = matrix
        # 计算谱半径和正特征向量
        self.perron_root, self.perron_vector = self.perron_frobenius()

    # 定义一个计算谱半径和正特征向量的方法
    def perron_frobenius(self):
        # 使用幂法求解最大特征值和特征向量
        # 初始化一个随机向量
        x = torch.rand(self.matrix.shape[0])
        # 设置一个容差
        tol = 1e-6
        # 设置一个最大迭代次数
        max_iter = 1000
        # 初始化一个误差
        error = np.inf
        # 初始化一个迭代次数
        iter = 0
        # 循环直到误差小于容差或达到最大迭代次数
        while error > tol and iter < max_iter:
            # 计算矩阵乘向量
            y = self.matrix @ x
            # 计算向量的范数
            norm = torch.norm(y)
            # 归一化向量
            y = y / norm
            # 计算误差
            error = torch.norm(x - y)
            # 更新向量
            x = y
            # 更新迭代次数
            iter += 1
        # 返回谱半径和正特征向量
        return norm, x

# 定义一个池化函数
def perron_frobenius_pooling(input, kernel_size):
    # 检查输入张量的维度
    assert input.ndim == 4, "The input tensor must have 4 dimensions."
    # 获取输入张量的形状
    batch_size, channels, height, width = input.shape
    # 检查池化核的大小
    assert kernel_size > 0 and kernel_size <= min(height, width), "The kernel size must be positive and smaller than the input height and width."
    # 计算输出张量的高度和宽度
    output_height = height // kernel_size
    output_width = width // kernel_size
    # 初始化一个输出张量
    output = torch.zeros(batch_size, channels, output_height, output_width)
    # 遍历每个批次
    for b in range(batch_size):
        # 遍历每个通道
        for c in range(channels):
            # 遍历每个池化区域
            for i in range(output_height):
                for j in range(output_width):
                    # 获取池化区域的矩阵
                    matrix = input[b, c, i * kernel_size : (i + 1) * kernel_size, j * kernel_size : (j + 1) * kernel_size]
                    # 创建一个非负矩阵的对象
                    nn_matrix = NonnegativeMatrix(matrix)
                    # 获取谱半径和正特征向量
                    perron_root, perron_vector = nn_matrix.perron_root, nn_matrix.perron_vector
                    # 将谱半径和正特征向量的平均值作为输出张量的元素
                    output[b, c, i, j] = (perron_root + torch.mean(perron_vector)) / 2
    # 返回输出张量
    return output

# 测试池化函数
# 创建一个随机输入张量
input = torch.rand(2, 3, 8, 8)
# 设置一个池化核的大小
kernel_size = 2
# 调用池化函数
output = perron_frobenius_pooling(input, kernel_size)
# 打印输出张量的形状和值
print(output.shape)
print(output)
