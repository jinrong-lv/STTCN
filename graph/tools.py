import numpy as np


def get_sgp_mat(num_in, num_out, link):
    A = np.zeros((num_in, num_out))
    for i, j in link:
        A[i, j] = 1
    A_norm = A / np.sum(A, axis=0, keepdims=True)
    return A_norm


def edge2mat(link, num_node):
    A = np.zeros((num_node, num_node))
    for i, j in link:
        A[j, i] = 1
    return A


def get_k_scale_graph(scale, A):
    if scale == 1:
        return A
    An = np.zeros_like(A)
    A_power = np.eye(A.shape[0])
    for k in range(scale):
        A_power = A_power @ A
        An += A_power
    An[An > 0] = 1
    return An


def normalize_digraph(A):
    Dl = np.sum(A, 0)
    h, w = A.shape
    Dn = np.zeros((w, w))
    for i in range(w):
        if Dl[i] > 0:
            Dn[i, i] = Dl[i] ** (-1)
    AD = np.dot(A, Dn)
    return AD


def get_spatial_graph(num_node, self_link, inward, outward):
    I = edge2mat(self_link, num_node)
    In = normalize_digraph(edge2mat(inward, num_node))
    Out = normalize_digraph(edge2mat(outward, num_node))
    A = np.stack((I, In, Out))
    return A


def get_ins_spatial_graph(num_node, self_link, inward, outward):
    I = edge2mat(self_link, num_node)
    In = normalize_digraph(np.ones_like(I) - I)
    Out = In.T
    A = np.stack((I, In, Out))
    return A


def get_spatial_graphnextv2(num_node, self_link, inward, outward):
    I = edge2mat(self_link, num_node)
    # In = normalize_digraph(edge2mat(inward, num_node))
    # Out = normalize_digraph(edge2mat(outward, num_node))
    # SELF = np.eye(num_node)
    A = np.stack((I, I, I, I))
    return A


def get_spatial_graphnext(num_node, self_link, inward, outward):
    I = edge2mat(self_link, num_node)
    In = normalize_digraph(edge2mat(inward, num_node))
    Out = normalize_digraph(edge2mat(outward, num_node))
    SELF = np.eye(num_node)
    A = np.stack((I, In, Out, SELF))
    return A


def normalize_adjacency_matrix(A):
    node_degrees = A.sum(-1)
    degs_inv_sqrt = np.power(node_degrees, -0.5)
    norm_degs_matrix = np.eye(len(node_degrees)) * degs_inv_sqrt
    return (norm_degs_matrix @ A @ norm_degs_matrix).astype(np.float32)


def k_adjacency(A, k, with_self=False, self_factor=1):
    assert isinstance(A, np.ndarray)
    I = np.eye(len(A), dtype=A.dtype)
    if k == 0:
        return I
    Ak = np.minimum(np.linalg.matrix_power(A + I, k), 1) \
         - np.minimum(np.linalg.matrix_power(A + I, k - 1), 1)
    if with_self:
        Ak += (self_factor * I)
    return Ak


def get_multiscale_spatial_graph(num_node, self_link, inward, outward):
    I = edge2mat(self_link, num_node)
    A1 = edge2mat(inward, num_node)
    A2 = edge2mat(outward, num_node)
    A3 = k_adjacency(A1, 2)
    A4 = k_adjacency(A2, 2)
    A1 = normalize_digraph(A1)
    A2 = normalize_digraph(A2)
    A3 = normalize_digraph(A3)
    A4 = normalize_digraph(A4)
    A = np.stack((I, A1, A2, A3, A4))
    return A


def get_uniform_graph(num_node, self_link, neighbor):
    A = normalize_digraph(edge2mat(neighbor + self_link, num_node))
    return A


def get_incidence_matrix(num_nodes, combinatorial_complex_lists):
    # 初始化一个全零的关联矩阵
    incidence_matrix = np.zeros((num_nodes, num_nodes))

    # 遍历复合体关联列表
    for cell, node_pairs in combinatorial_complex_lists.items():
        for node_pair in node_pairs:
            # 对于每个节点对，将关联矩阵中对应的位置设为1
            incidence_matrix[node_pair[0], cell] = 1
            incidence_matrix[node_pair[1], cell] = 1

    return incidence_matrix


# 通过节点的度数归一化关联矩阵
def normalize_incidence_matrix_by_degree(incidence_matrix):
    # 计算每个节点的度数（即每行的和）
    degree = np.sum(np.abs(incidence_matrix), axis=1)
    # 创建对角度矩阵
    D = np.diag(degree)
    # 计算D的逆或伪逆以避免除以零
    D_inv = np.linalg.pinv(D)
    # 归一化关联矩阵
    normalized_matrix = np.dot(D_inv, incidence_matrix)
    return normalized_matrix


# 使用z-score标准化关联矩阵
def normalize_incidence_matrix_by_zscore(incidence_matrix):
    # 计算关联矩阵的均值和标准差
    mean = np.mean(incidence_matrix, axis=0)
    std = np.std(incidence_matrix, axis=0)
    # 防止除以零
    std[std == 0] = 1
    # 计算z-score标准化后的关联矩阵
    normalized_matrix = (incidence_matrix - mean) / std
    return normalized_matrix


def get_incidence_matrix(num_nodes, combinatorial_complex_lists):
    # 计算复合体的总数
    num_complexes = sum(len(complex_list) for complex_list in combinatorial_complex_lists.values())

    # 初始化一个全零的关联矩阵，行数为节点数，列数为复合体数
    incidence_matrix = np.zeros((num_nodes, num_complexes))

    # 复合体索引计数器
    complex_index = 0

    # 遍历复合体关联列表
    for complex_name, node_lists in combinatorial_complex_lists.items():
        for node_list in node_lists:
            # 对于每个节点列表，将所有节点与该复合体相关联
            for node in node_list:
                incidence_matrix[node, complex_index] = 1
            # 更新复合体索引
            complex_index += 1

    return incidence_matrix


def get_directed_incidence_matrix(num_nodes, combinatorial_complex_lists):
    # 计算复合体的总数
    num_complexes = sum(len(complex_list) for complex_list in combinatorial_complex_lists.values())

    # 初始化一个全零的关联矩阵，行数为节点数，列数为复合体数
    incidence_matrix = np.zeros((num_nodes, num_complexes))

    # 复合体索引计数器
    complex_index = 0

    # 遍历复合体关联列表
    for complex_name, node_lists in combinatorial_complex_lists.items():
        for node_list in node_lists:
            # 假设第一个节点是起点，其余节点是终点
            incidence_matrix[node_list[0], complex_index] = 1  # 起点
            for node in node_list[1:]:
                incidence_matrix[node, complex_index] = -1  # 终点
            # 更新复合体索引
            complex_index += 1

    return incidence_matrix


def get_incidence_graph(num_nodes, combinatorial_complex_lists):
    self_link = [(i, i) for i in range(num_nodes)]
    B_in = normalize_incidence_matrix_by_degree(get_incidence_matrix(num_nodes, combinatorial_complex_lists))
    # B_out = normalize_incidence_matrix_by_zscore(get_incidence_matrix(num_nodes, combinatorial_complex_lists))
    # B_out = normalize_incidence_matrix_by_degree(get_directed_incidence_matrix(num_nodes, combinatorial_complex_lists))
    # B_out = B_in.T
    I = normalize_adjacency_matrix(edge2mat(self_link, num_nodes))
    J = normalize_adjacency_matrix(np.ones_like(B_in))

    B = np.stack((I, B_in, J))

    return B


if __name__ == '__main__':
    # 节点数目
    num_nodes = 25
    # 复合体关联字典
    combinatorial_complex_lists = {
        # 头部
        'head': [(3, 2)],
        # 颈部
        'neck': [(2, 20)],
        # 腹部
        'abdomen': [(8, 20, 4, 1, 16, 0, 12)],
        # 全身
        'body': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 1, 12, 13, 14, 1, 3, 2, 1)],
        # 四肢
        'arms_and_legs': [(24, 23, 11, 10, 9, 8, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 12, 13, 14, 15)],
        # 左右手
        'left_and_right_hands': [(24, 23, 11, 10, 6, 7, 21, 22)],
        # 左右脚
        'left_and_right_feets': [(19, 18, 15, 14)],
        # 左右手臂
        'left_and_right_arms': [(10, 9, 8, 4, 5, 6)],
        # 左右腿
        'left_and_right_thighs': [(17, 16, 13, 12)],
        # 左半边身体
        'left_body': [(24, 23, 11, 10, 9, 8, 20, 1, 19, 20, 17, 16, 0)],
        # 右半边身体
        'right_body': [(20, 4, 5, 6, 7, 21, 22, 1, 0, 12, 13, 14, 15)],
        # 上躯干
        'upper_torso': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22)],
        # 左上臂
        'left_upper_arm': [(24, 23, 11, 10, 9, 8, 20)],
        # 右上臂
        'right_upper_arm': [(20, 4, 5, 6, 7, 21, 22)],
        # 左前臂
        'left_forearm': [(24, 23, 11, 10, 9)],
        # 右前臂
        'right_forearm': [(5, 6, 7, 21, 22)],
        # 左手
        'left_hand': [(24, 23, 11, 10)],
        # 右手
        'right_hand': [(6, 7, 21, 22)],
        # 下躯干
        'lower_torso': [(19, 18, 17, 16, 0, 12, 13, 14, 15)],
        # 左大腿
        'left_thigh': [(18, 17, 0)],
        # 右大腿
        'right_thigh': [(13, 12, 0)],
        # 左小腿
        'left_calf': [(19, 18, 17)],
        # 右小腿
        'right_calf': [(15, 14, 13)],
        # 左脚
        'left_foot': [(19, 18)],
        # 右脚
        'right_foot': [(15, 14)]
    }

    total_complexes = sum(len(complexes) for complexes in combinatorial_complex_lists.values())
    print("复合体数量：", total_complexes)

    # 调用函数创建关联矩阵
    import time

    start_time = time.perf_counter()
    # incidence_matrix = normalize_incidence_matrix_by_degree(get_directed_incidence_matrix(num_nodes, combinatorial_complex_lists))
    A = get_incidence_graph(num_nodes, combinatorial_complex_lists)
    # incidence_matrix = get_incidence_matrix(num_nodes, combinatorial_complex_lists)

    # incidence_matrix = normalize_incidence_matrix_by_degree(incidence_matrix)
    # incidence_matrix = normalize_incidence_matrix_by_zscore(incidence_matrix)

    end_time = time.perf_counter()
    print(f"运行时间：{end_time - start_time} 秒")

    print(A)
