import sys
import numpy as np

sys.path.extend(['../'])
from graph import tools

num_node = 25
self_link = [(i, i) for i in range(num_node)]
inward_ori_index = [(1, 2), (2, 21), (3, 21), (4, 3), (5, 21), (6, 5), (7, 6),
                    (8, 7), (9, 21), (10, 9), (11, 10), (12, 11), (13, 1),
                    (14, 13), (15, 14), (16, 15), (17, 1), (18, 17), (19, 18),
                    (20, 19), (22, 23), (23, 8), (24, 25), (25, 12)]
inward = [(i - 1, j - 1) for (i, j) in inward_ori_index]
outward = [(j, i) for (i, j) in inward]
neighbor = inward + outward

num_node_1 = 11
indices_1 = [0, 3, 5, 7, 9, 11, 13, 15, 17, 19, 20]
self_link_1 = [(i, i) for i in range(num_node_1)]
inward_ori_index_1 = [(1, 11), (2, 11), (3, 11), (4, 3), (5, 11), (6, 5), (7, 1), (8, 7), (9, 1), (10, 9)]
inward_1 = [(i - 1, j - 1) for (i, j) in inward_ori_index_1]
outward_1 = [(j, i) for (i, j) in inward_1]
neighbor_1 = inward_1 + outward_1

num_node_2 = 5
indices_2 = [3, 5, 6, 8, 10]
self_link_2 = [(i ,i) for i in range(num_node_2)]
inward_ori_index_2 = [(0, 4), (1, 4), (2, 4), (3, 4), (0, 1), (2, 3)]
inward_2 = [(i - 1, j - 1) for (i, j) in inward_ori_index_2]
outward_2 = [(j, i) for (i, j) in inward_2]
neighbor_2 = inward_2 + outward_2

# 复合体关联字典
combinatorial_complex_lists = {
    # 头部
    'head': [(3, 2)],
    # 颈部
    'neck': [(2, 20)],
    # 腹部
    'abdomen': [(8, 20, 4, 1, 16, 0, 12)],
    # 全身
    'body': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 1, 12, 13, 14, 1, 3, 2, 1)],
    # 四肢
    'arms_and_legs': [(24, 23, 11, 10, 9, 8, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 12, 13, 14, 15)],
    # 左右手
    'left_and_right_hands': [(24, 23, 11, 10, 6, 7, 21, 22)],
    # 左右脚
    'left_and_right_feets': [(19, 18, 15, 14)],
    # 左右手臂
    'left_and_right_arms': [(10, 9, 8, 4, 5, 6)],
    # 左右腿
    'left_and_right_thighs': [(17, 16, 13, 12)],
    # 左半边身体
    'left_body': [(24, 23, 11, 10, 9, 8, 20, 1, 19, 20, 17, 16, 0)],
    # 右半边身体
    'right_body': [(20, 4, 5, 6, 7, 21, 22, 1, 0, 12, 13, 14, 15)],
    # 上躯干
    'upper_torso': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22)],
    # 左上臂
    'left_upper_arm': [(24, 23, 11, 10, 9, 8, 20)],
    # 右上臂
    'right_upper_arm': [(20, 4, 5, 6, 7, 21, 22)],
    # 左前臂
    'left_forearm': [(24, 23, 11, 10, 9)],
    # 右前臂
    'right_forearm': [(5, 6, 7, 21, 22)],
    # 左手
    'left_hand': [(24, 23, 11, 10)],
    # 右手
    'right_hand': [(6, 7, 21, 22)],
    # 下躯干
    'lower_torso': [(19, 18, 17, 16, 0, 12, 13, 14, 15)],
    # 左大腿
    'left_thigh': [(18, 17, 0)],
    # 右大腿
    'right_thigh': [(13, 12, 0)],
    # 左小腿
    'left_calf': [(19, 18, 17)],
    # 右小腿
    'right_calf': [(15, 14, 13)],
    # 左脚
    'left_foot': [(19, 18)],
    # 右脚
    'right_foot': [(15, 14)]
}

class Graph:
    def __init__(self, labeling_mode='spatial', scale=1):
        self.num_node = num_node
        self.self_link = self_link
        self.inward = inward
        self.outward = outward
        self.neighbor = neighbor
        self.A = self.get_adjacency_matrix(labeling_mode)
        self.A1 = tools.get_spatial_graph(num_node_1, self_link_1, inward_1, outward_1)
        self.A2 = tools.get_spatial_graph(num_node_2, self_link_2, inward_2, outward_2)
        self.A_binary = tools.edge2mat(neighbor, num_node)
        self.A_norm = tools.normalize_adjacency_matrix(self.A_binary + 2*np.eye(num_node))
        self.A_binary_K = tools.get_k_scale_graph(scale, self.A_binary)

        self.A_A1 = ((self.A_binary + np.eye(num_node)) / np.sum(self.A_binary + np.eye(self.A_binary.shape[0]), axis=1, keepdims=True))[indices_1]
        self.A1_A2 = tools.edge2mat(neighbor_1, num_node_1) + np.eye(num_node_1)
        self.A1_A2 = (self.A1_A2 / np.sum(self.A1_A2, axis=1, keepdims=True))[indices_2]


    def get_adjacency_matrix(self, labeling_mode=None):
        if labeling_mode is None:
            return self.A
        if labeling_mode == 'spatial':
            A = tools.get_spatial_graph(num_node, self_link, inward, outward)
        elif labeling_mode == 'spatialnext':
            A = tools.get_spatial_graphnext(num_node, self_link, inward, outward)
        elif labeling_mode == 'spatial_intensive':
            A = tools.get_ins_spatial_graph(num_node, self_link, inward, outward)
        elif labeling_mode == 'combinatorial_complex':
            A = tools.get_incidence_graph(num_node, combinatorial_complex_lists)
        else:
            raise ValueError()
        return A
