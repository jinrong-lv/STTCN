import numpy as np
from scipy.sparse import csr_matrix


# # 定义一个计算图的度中心性的函数
# def degree_centrality(adj_matrix):
#     # 获取邻接矩阵的维度，即节点的数量
#     n = adj_matrix.shape[0]
#     # 初始化一个空的矩阵，用于存储每个节点的度中心性
#     degree = np.zeros((n, n))
#     # 遍历每个节点
#     for i in range(n):
#         # 计算该节点的度数，即邻接矩阵的第i行的非零元素的个数
#         deg = np.count_nonzero(adj_matrix[i])
#         # 计算该节点的度中心性，即度数除以节点数减1
#         degree[i, i] = deg / (n - 1)
#     # 对度中心性进行归一化，使其在0到1之间
#     # 参考 [归一化数据 - MATLAB normalize - MathWorks](https://zhuanlan.zhihu.com/p/145601101) 和 [数据归一化 - 知乎](https://www.nebula-graph.com.cn/posts/introduction-to-betweenness-centrality-algorithm)
#     # degree = (degree - np.min(degree)) / (np.max(degree) - np.min(degree))
#     # 返回度中心性的矩阵
#     return degree



def degree_centrality(adj_matrix):
    # adj_matrix is a 25 x 25 numpy array
    # degree centrality is a 25 x 25 numpy array
    degree = np.sum(adj_matrix, axis=1)  # sum along rows to get degree of each node
    n = adj_matrix.shape[0]  # number of nodes
    degree_centrality = degree / (n - 1)  # normalize by n - 1
    degree_centrality = np.diag(degree_centrality)  # create a diagonal matrix with degree centrality values
    return degree_centrality


# 定义一个计算图的接近中心性的函数(edge)
def closeness_centrality(adj_matrix):
    # 获取邻接矩阵的维度，即节点的数量
    n = adj_matrix.shape[0]
    # 初始化一个空的矩阵，用于存储每个节点的接近中心性
    closeness = np.zeros((n, n))
    # 遍历每个节点
    for i in range(n):
        # 初始化一个空的字典，用于存储该节点到其他节点的最短距离
        dist = {}
        # 初始化一个队列，用于进行广度优先搜索
        queue = []
        # 将该节点自身加入队列，并将其距离设为0
        queue.append(i)
        dist[i] = 0
        # 当队列不为空时，循环执行以下操作
        while queue:
            # 从队列中弹出一个节点
            u = queue.pop(0)
            # 遍历邻接矩阵的第u行，即与u相邻的节点
            for v in range(n):
                # 如果v不在dist中，且邻接矩阵的[u,v]元素不为0，即u和v之间有边
                if v not in dist and adj_matrix[u,v] != 0:
                    # 将v加入队列，并将其距离设为u的距离加1
                    queue.append(v)
                    dist[v] = dist[u] + 1
        # 计算该节点到其他节点的距离之和
        sum_dist = sum(dist.values())
        # 如果距离之和为0，说明该节点是孤立的，那么其接近中心性为0
        if sum_dist == 0:
            closeness[i, i] = 0
        # 否则，计算该节点的接近中心性，即节点数减1除以距离之和
        else:
            closeness[i, i] = (n-1) / sum_dist
    # 对接近中心性进行归一化，使其在0到1之间
    # 参考 [归一化数据 - MATLAB normalize - MathWorks](https://www.mathworks.com/help/matlab/ref/double.normalize_zh_CN.html) 和 [数据归一化 - 知乎](https://zhuanlan.zhihu.com/p/76682561)
    closeness = (closeness - np.min(closeness)) / (np.max(closeness) - np.min(closeness))
    # n = adj_matrix.shape[0]
    # closeness = closeness / (n - 1)
    # 返回接近中心性的矩阵
    return closeness


# # 定义一个计算图的介数中心性的函数(edge)
# def betweenness_centrality(adj_matrix):
#     # 获取邻接矩阵的维度，即节点的数量
#     n = adj_matrix.shape[0]
#     # 初始化一个空的矩阵，用于存储每个节点的介数中心性
#     betweenness = np.zeros((n, n))
#     # 遍历每个节点作为源节点
#     for s in range(n):
#         # 初始化一个空的栈，用于存储最短路径搜索的顺序
#         stack = []
#         # 初始化一个空的列表，用于存储每个节点的前驱节点
#         pred = [[] for i in range(n)]
#         # 初始化一个列表，用于存储每个节点到源节点的最短距离
#         dist = [-1] * n
#         # 初始化一个列表，用于存储每个节点到源节点的最短路径条数
#         sigma = [0] * n
#         # 将源节点到自身的距离设为0
#         dist[s] = 0
#         # 将源节点到自身的最短路径条数设为1
#         sigma[s] = 1
#         # 初始化一个队列，用于进行广度优先搜索
#         queue = []
#         # 将源节点加入队列
#         queue.append(s)
#         # 当队列不为空时，循环执行以下操作
#         while queue:
#             # 从队列中弹出一个节点
#             v = queue.pop(0)
#             # 将该节点加入栈
#             stack.append(v)
#             # 遍历邻接矩阵的第v行，即与v相邻的节点
#             for w in range(n):
#                 # 如果邻接矩阵的[v,w]元素不为0，即v和w之间有边
#                 if adj_matrix[v,w] != 0:
#                     # 如果w的距离为-1，说明w还没有被访问过
#                     if dist[w] < 0:
#                         # 将w加入队列
#                         queue.append(w)
#                         # 将w的距离设为v的距离加1
#                         dist[w] = dist[v] + 1
#                     # 如果w的距离等于v的距离加1，说明w是v的后继节点
#                     if dist[w] == dist[v] + 1:
#                         # 将v加入w的前驱节点列表
#                         pred[w].append(v)
#                         # 将w的最短路径条数增加v的最短路径条数
#                         sigma[w] += sigma[v]
#         # 初始化一个列表，用于存储每个节点的依赖度
#         delta = [0] * n
#         # 当栈不为空时，循环执行以下操作
#         while stack:
#             # 从栈中弹出一个节点
#             w = stack.pop()
#             # 遍历w的前驱节点列表
#             for v in pred[w]:
#                 # 计算v对w的依赖度
#                 c = sigma[v] / sigma[w] * (1 + delta[w])
#                 # 将v的依赖度增加c
#                 delta[v] += c
#             # 如果w不是源节点，将w的介数中心性增加w的依赖度
#             if w != s:
#                 betweenness[w, w] += delta[w]
#     # 对介数中心性进行归一化，使其在0到1之间
#     # 参考 [归一化数据 - MATLAB normalize - MathWorks](https://www.nebula-graph.com.cn/posts/introduction-to-betweenness-centrality-algorithm) 和 [数据归一化 - 知乎](https://zhuanlan.zhihu.com/p/145601101)
#     # betweenness = (betweenness - np.min(betweenness)) / (np.max(betweenness) - np.min(betweenness))
#     betweenness = betweenness / ((n - 1) * (n - 2))
#     # 返回介数中心性的矩阵
#     return betweenness



def betweenness_centrality(adj_matrix):
    # adj_matrix is a 25 x 25 numpy array
    # betweenness centrality is a 25 x 25 numpy array
    n = adj_matrix.shape[0]  # number of nodes
    dist_matrix = np.full((n, n), np.inf)  # initialize distance matrix with infinity
    np.fill_diagonal(dist_matrix, 0)  # set diagonal to zero
    dist_matrix[adj_matrix > 0] = 1  # set distance to one for adjacent nodes
    path_matrix = np.zeros((n, n, n))  # initialize path matrix with zeros
    path_matrix[adj_matrix > 0] = 1  # set path count to one for adjacent nodes
    for k in range(n):  # use Floyd-Warshall algorithm to find shortest paths and count them
        for i in range(n):
            for j in range(n):
                if dist_matrix[i, j] > dist_matrix[i, k] + dist_matrix[
                    k, j]:  # update distance and path count if a shorter path is found
                    dist_matrix[i, j] = dist_matrix[i, k] + dist_matrix[k, j]
                    path_matrix[i, j] = path_matrix[i, k] * path_matrix[k, j]
                elif dist_matrix[i, j] == dist_matrix[i, k] + dist_matrix[
                    k, j]:  # add path count if another shortest path is found
                    path_matrix[i, j] += path_matrix[i, k] * path_matrix[k, j]
    betweenness_centrality = np.zeros(n)  # initialize betweenness centrality with zeros
    for i in range(n):  # loop over all nodes
        for j in range(n):  # loop over all pairs of nodes
            if i != j:
                for k in range(n):  # loop over all intermediate nodes
                    if k != i and k != j:  # exclude endpoints
                        if path_matrix[i, j, k] > 0:  # if node k is on a shortest path from i to j
                            betweenness_centrality[k] += path_matrix[i, k, k] * path_matrix[k, j, k] / path_matrix[
                                i, j, k]  # add the fraction of shortest paths that go through node k
    betweenness_centrality = betweenness_centrality / ((n - 1) * (n - 2))  # normalize by (n - 1) * (n - 2)
    betweenness_centrality = np.diag(
        betweenness_centrality)  # create a diagonal matrix with betweenness centrality values
    return betweenness_centrality


def get_sgp_mat(num_in, num_out, link):
    A = np.zeros((num_in, num_out))
    for i, j in link:
        A[i, j] = 1
    A_norm = A / np.sum(A, axis=0, keepdims=True)
    return A_norm


def edge2mat(link, num_node):
    A = np.zeros((num_node, num_node))
    for i, j in link:
        A[j, i] = 1
    return A


def get_k_scale_graph(scale, A):
    if scale == 1:
        return A
    An = np.zeros_like(A)
    A_power = np.eye(A.shape[0])
    for k in range(scale):
        A_power = A_power @ A
        An += A_power
    An[An > 0] = 1
    return An


def normalize_digraph(A):
    Dl = np.sum(A, 0)
    h, w = A.shape
    Dn = np.zeros((w, w))
    for i in range(w):
        if Dl[i] > 0:
            Dn[i, i] = Dl[i] ** (-1)
    AD = np.dot(A, Dn)
    return AD


def get_spatial_graph(num_node, self_link, inward, outward):
    I = edge2mat(self_link, num_node)
    In = normalize_digraph(edge2mat(inward, num_node))
    Out = normalize_digraph(edge2mat(outward, num_node))

    # I += degree_centrality(I)
    # In += closeness_centrality(In)
    # Out += betweenness_centrality(Out)

    A = np.stack((I, In, Out))
    return A


def normalize_adjacency_matrix(A):
    node_degrees = A.sum(-1)
    degs_inv_sqrt = np.power(node_degrees, -0.5)
    norm_degs_matrix = np.eye(len(node_degrees)) * degs_inv_sqrt
    return (norm_degs_matrix @ A @ norm_degs_matrix).astype(np.float32)


def k_adjacency(A, k, with_self=False, self_factor=1):
    assert isinstance(A, np.ndarray)
    I = np.eye(len(A), dtype=A.dtype)
    if k == 0:
        return I
    Ak = np.minimum(np.linalg.matrix_power(A + I, k), 1) \
         - np.minimum(np.linalg.matrix_power(A + I, k - 1), 1)
    if with_self:
        Ak += (self_factor * I)
    return Ak


def get_multiscale_spatial_graph(num_node, self_link, inward, outward):
    I = edge2mat(self_link, num_node)
    A1 = edge2mat(inward, num_node)
    A2 = edge2mat(outward, num_node)
    A3 = k_adjacency(A1, 2)
    A4 = k_adjacency(A2, 2)
    A1 = normalize_digraph(A1)
    A2 = normalize_digraph(A2)
    A3 = normalize_digraph(A3)
    A4 = normalize_digraph(A4)
    A = np.stack((I, A1, A2, A3, A4))
    return A


def get_uniform_graph(num_node, self_link, neighbor):
    A = normalize_digraph(edge2mat(neighbor + self_link, num_node))
    return A


def get_all_1_hop_subgraphs(link, num_node):
    # 邻接列表to邻接矩阵
    adj_matrix = edge2mat(link, num_node)
    # 获取节点数
    num_nodes = adj_matrix.shape[0]
    # 找到所有连接的节点对
    row_indices, col_indices = np.nonzero(adj_matrix)

    # 构建一个包含所有子图的列表
    all_subgraphs = []

    for node_idx in range(num_nodes):
        # 找到与当前节点相连接的节点索引
        connected_nodes = np.unique(np.concatenate((row_indices[col_indices == node_idx],
                                                    col_indices[row_indices == node_idx])))

        # 构建子图邻接矩阵
        subgraph_matrix = np.zeros_like(adj_matrix)
        subgraph_matrix[node_idx, connected_nodes] = 1
        subgraph_matrix[connected_nodes, node_idx] = 1

        all_subgraphs.append(subgraph_matrix)

    # # 将所有子图堆叠在一起以创建一个大矩阵
    all_subgraphs_array = np.stack(all_subgraphs)

    return all_subgraphs_array


def get_subgraphs_and_centrality(link, num_node):
    # 邻接列表to邻接矩阵
    adj_matrix = edge2mat(link, num_node)
    # decompose_matrix = np.ones((num_node, num_node))
    decompose_matrix = adj_matrix
    # 获取节点数
    num_nodes = decompose_matrix.shape[0]
    # 找到所有连接的节点对
    row_indices, col_indices = np.nonzero(decompose_matrix)

    # 构建一个包含所有子图的列表
    all_subgraphs = []

    for node_idx in range(num_nodes):
        # 找到与当前节点相连接的节点索引
        connected_nodes = np.unique(np.concatenate((row_indices[col_indices == node_idx],
                                                    col_indices[row_indices == node_idx])))

        # 构建子图邻接矩阵
        subgraph_matrix = np.zeros_like(decompose_matrix)
        subgraph_matrix[node_idx, connected_nodes] = 1
        subgraph_matrix[connected_nodes, node_idx] = 1

        # 加上中心性
        subgraph_matrix += degree_centrality(adj_matrix)
        # subgraph_matrix += closeness_centrality(adj_matrix)
        subgraph_matrix += betweenness_centrality(adj_matrix)

        # 归一化
        # subgraph_matrix = normalize_digraph(subgraph_matrix)
        # subgraph_matrix = normalize_adjacency_matrix(subgraph_matrix)

        all_subgraphs.append(subgraph_matrix)

    # # 将所有子图堆叠在一起以创建一个大矩阵
    all_subgraphs_array = np.stack(all_subgraphs)

    return all_subgraphs_array


if __name__ == '__main__':
    num_node = 25
    self_link = [(i, i) for i in range(num_node)]
    inward_ori_index = [(1, 2), (2, 21), (3, 21), (4, 3), (5, 21), (6, 5), (7, 6),
                        (8, 7), (9, 21), (10, 9), (11, 10), (12, 11), (13, 1),
                        (14, 13), (15, 14), (16, 15), (17, 1), (18, 17), (19, 18),
                        (20, 19), (22, 23), (23, 8), (24, 25), (25, 12)]
    inward = [(i - 1, j - 1) for (i, j) in inward_ori_index]
    outward = [(j, i) for (i, j) in inward]

    # A = get_multiscale_spatial_graph(num_node,self_link, inward, outward)
    # A = get_spatial_graph(num_node,self_link, inward, outward)
    # A = get_subgraphs_and_centrality(inward, num_node)
    A = edge2mat(inward, num_node)
    # A = betweenness_centrality(A) + degree_centrality(A)
    A = get_subgraphs_and_centrality(inward, num_node)
    # A = get_spatial_graph(num_node, self_link, inward, outward)
    # A = betweenness_centrality(A)
    print(A)
