import matplotlib.pyplot as plt
import matplotlib.patches as patches

# 复合体关联字典
combinatorial_complex_lists = {
    # 头部
    'head': [(3, 2)],
    # 颈部
    'neck': [(2, 20)],
    # 腹部
    'abdomen': [(8, 20, 4, 1, 16, 0, 12)],
    # 全身
    'body': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 1, 12, 13, 14, 1, 3, 2, 1)],
    # 四肢
    'arms_and_legs': [(24, 23, 11, 10, 9, 8, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 12, 13, 14, 15)],
    # 左右手
    'left_and_right_hands': [(24, 23, 11, 10, 6, 7, 21, 22)],
    # 左右脚
    'left_and_right_feets': [(19, 18, 15, 14)],
    # 左右手臂
    'left_and_right_arms': [(10, 9, 8, 4, 5, 6)],
    # 左右腿
    'left_and_right_thighs': [(17, 16, 13, 12)],
    # 左半边身体
    'left_body': [(24, 23, 11, 10, 9, 8, 20, 1, 19, 20, 17, 16, 0)],
    # 右半边身体
    'right_body': [(20, 4, 5, 6, 7, 21, 22, 1, 0, 12, 13, 14, 15)],
    # 上躯干
    'upper_torso': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22)],
    # 左上臂
    'left_upper_arm': [(24, 23, 11, 10, 9, 8, 20)],
    # 右上臂
    'right_upper_arm': [(20, 4, 5, 6, 7, 21, 22)],
    # 左前臂
    'left_forearm': [(24, 23, 11, 10, 9)],
    # 右前臂
    'right_forearm': [(5, 6, 7, 21, 22)],
    # 左手
    'left_hand': [(24, 23, 11, 10)],
    # 右手
    'right_hand': [(6, 7, 21, 22)],
    # 下躯干
    'lower_torso': [(19, 18, 17, 16, 0, 12, 13, 14, 15)],
    # 左大腿
    'left_thigh': [(18, 17, 0)],
    # 右大腿
    'right_thigh': [(13, 12, 0)],
    # 左小腿
    'left_calf': [(19, 18, 17)],
    # 右小腿
    'right_calf': [(15, 14, 13)],
    # 左脚
    'left_foot': [(19, 18)],
    # 右脚
    'right_foot': [(15, 14)]
}

# 关节的坐标
points = {
    0: (7, 10),  # 腹部
    1: (7, 12),  # 腹部上方
    2: (7, 15),  # 颈部
    3: (7, 16),  # 头部
    4: (8, 14),  # 左侧腹部
    5: (9.5, 15.5),  # 右侧腹部
    6: (11, 16),  # 左上臂
    7: (11.5, 16.5),  # 右上臂
    8: (6, 14),  # 左前臂
    9: (3.5, 15.5),  # 右前臂
    10: (2.5, 16),  # 左手
    11: (1.5, 16.5),  # 右手
    12: (8, 10),  # 下躯干
    13: (9, 5.5),  # 左大腿
    14: (9.5, 3),  # 右大腿
    15: (10, 1),  # 左小腿
    16: (6, 10),  # 右小腿
    17: (4, 5.5),  # 左脚
    18: (3.5, 3),  # 右脚
    19: (3, 1),  # 左脚尖
    20: (7, 14),  # 右脚尖
    21: (12, 17),  # 左手尖
    22: (11.5, 16.5),  # 右手尖
    23: (0.5, 17),  # 左头顶
    24: (1, 16.5)   # 右头顶
}

body_flow_list = [24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22, 7, 6, 5, 4, 20, 1, 0, 16, 17, 18, 19,
             18, 17, 16, 0, 12, 13, 14, 15]


# 初始化一个图形和坐标轴
fig, ax = plt.subplots()

# 设置图表标题和坐标轴标签
ax.set_title('Human Body Parts Visualization')
ax.set_xlabel('X Coordinate')
ax.set_ylabel('Y Coordinate')

# 扩充颜色列表，每个复合体一个颜色
colors = plt.cm.get_cmap('tab20', 25).colors

# 遍历复合体关联字典，绘制每个身体部位
for index, (part, node_groups) in enumerate(combinatorial_complex_lists.items()):
    for node_group in node_groups:
        # 获取每个节点的坐标
        x_coords = [points[node][0] for node in node_group]
        y_coords = [points[node][1] for node in node_group]
        # 创建多边形并添加到坐标轴上
        polygon = patches.Polygon(list(zip(x_coords, y_coords)), alpha=0.3, color=colors[index], label=part)
        ax.add_patch(polygon)

# 特别绘制躯干，使用黄色粗线条
torso_nodes = body_flow_list
x_torso = [points[node][0] for node in torso_nodes]
y_torso = [points[node][1] for node in torso_nodes]
ax.plot(x_torso, y_torso, color='yellow', linewidth=3)

# 可视化关节点的序号
for node_index, (x, y) in points.items():
    ax.text(x, y, str(node_index), color='black', fontsize=12, ha='center', va='center')

# 设置图例位置在坐标轴外的右边
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

# 调整布局以防止图例被裁剪
plt.tight_layout()

# 显示图形
plt.show()



