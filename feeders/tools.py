import random
import matplotlib.pyplot as plt
import numpy as np
import pdb

import torch
import torch.nn.functional as F


def valid_crop_resize(data_numpy, valid_frame_num, p_interval, window):
    # input: C,T,V,M
    C, T, V, M = data_numpy.shape
    begin = 0
    end = valid_frame_num
    valid_size = end - begin

    # crop
    if len(p_interval) == 1:
        p = p_interval[0]
        bias = int((1 - p) * valid_size / 2)
        data = data_numpy[:, begin + bias:end - bias, :, :]  # center_crop
        cropped_length = data.shape[1]
    else:
        p = np.random.rand(1) * (p_interval[1] - p_interval[0]) + p_interval[0]
        cropped_length = np.minimum(np.maximum(int(np.floor(valid_size * p)), 64),
                                    valid_size)  # constraint cropped_length lower bound as 64
        bias = np.random.randint(0, valid_size - cropped_length + 1)
        data = data_numpy[:, begin + bias:begin + bias + cropped_length, :, :]
        if data.shape[1] == 0:
            print(cropped_length, bias, valid_size)

    # resize
    data = torch.tensor(data, dtype=torch.float)
    data = data.permute(0, 2, 3, 1).contiguous().view(C * V * M, cropped_length)
    data = data[None, None, :, :]
    data = F.interpolate(data, size=(C * V * M, window), mode='bilinear',
                         align_corners=False).squeeze()  # could perform both up sample and down sample
    data = data.contiguous().view(C, V, M, window).permute(0, 3, 1, 2).contiguous().numpy()

    return data


def downsample(data_numpy, step, random_sample=True):
    # input: C,T,V,M
    begin = np.random.randint(step) if random_sample else 0
    return data_numpy[:, begin::step, :, :]


def temporal_slice(data_numpy, step):
    # input: C,T,V,M
    C, T, V, M = data_numpy.shape
    return data_numpy.reshape(C, T / step, step, V, M).transpose(
        (0, 1, 3, 2, 4)).reshape(C, T / step, V, step * M)


def mean_subtractor(data_numpy, mean):
    # input: C,T,V,M
    # naive version
    if mean == 0:
        return
    C, T, V, M = data_numpy.shape
    valid_frame = (data_numpy != 0).sum(axis=3).sum(axis=2).sum(axis=0) > 0
    begin = valid_frame.argmax()
    end = len(valid_frame) - valid_frame[::-1].argmax()
    data_numpy[:, :end, :, :] = data_numpy[:, :end, :, :] - mean
    return data_numpy


def auto_pading(data_numpy, size, random_pad=False):
    C, T, V, M = data_numpy.shape
    if T < size:
        begin = random.randint(0, size - T) if random_pad else 0
        data_numpy_paded = np.zeros((C, size, V, M))
        data_numpy_paded[:, begin:begin + T, :, :] = data_numpy
        return data_numpy_paded
    else:
        return data_numpy


def random_choose(data_numpy, size, auto_pad=True):
    # input: C,T,V,M 随机选择其中一段，不是很合理。因为有0
    C, T, V, M = data_numpy.shape
    if T == size:
        return data_numpy
    elif T < size:
        if auto_pad:
            return auto_pading(data_numpy, size, random_pad=True)
        else:
            return data_numpy
    else:
        begin = random.randint(0, T - size)
        return data_numpy[:, begin:begin + size, :, :]


def random_move(data_numpy,
                angle_candidate=[-10., -5., 0., 5., 10.],
                scale_candidate=[0.9, 1.0, 1.1],
                transform_candidate=[-0.2, -0.1, 0.0, 0.1, 0.2],
                move_time_candidate=[1]):
    # input: C,T,V,M
    C, T, V, M = data_numpy.shape
    move_time = random.choice(move_time_candidate)
    node = np.arange(0, T, T * 1.0 / move_time).round().astype(int)
    node = np.append(node, T)
    num_node = len(node)

    A = np.random.choice(angle_candidate, num_node)
    S = np.random.choice(scale_candidate, num_node)
    T_x = np.random.choice(transform_candidate, num_node)
    T_y = np.random.choice(transform_candidate, num_node)

    a = np.zeros(T)
    s = np.zeros(T)
    t_x = np.zeros(T)
    t_y = np.zeros(T)

    # linspace
    for i in range(num_node - 1):
        a[node[i]:node[i + 1]] = np.linspace(
            A[i], A[i + 1], node[i + 1] - node[i]) * np.pi / 180
        s[node[i]:node[i + 1]] = np.linspace(S[i], S[i + 1],
                                             node[i + 1] - node[i])
        t_x[node[i]:node[i + 1]] = np.linspace(T_x[i], T_x[i + 1],
                                               node[i + 1] - node[i])
        t_y[node[i]:node[i + 1]] = np.linspace(T_y[i], T_y[i + 1],
                                               node[i + 1] - node[i])

    theta = np.array([[np.cos(a) * s, -np.sin(a) * s],
                      [np.sin(a) * s, np.cos(a) * s]])

    # perform transformation
    for i_frame in range(T):
        xy = data_numpy[0:2, i_frame, :, :]
        new_xy = np.dot(theta[:, :, i_frame], xy.reshape(2, -1))
        new_xy[0] += t_x[i_frame]
        new_xy[1] += t_y[i_frame]
        data_numpy[0:2, i_frame, :, :] = new_xy.reshape(2, V, M)

    return data_numpy


def random_shift(data_numpy):
    C, T, V, M = data_numpy.shape
    data_shift = np.zeros(data_numpy.shape)
    valid_frame = (data_numpy != 0).sum(axis=3).sum(axis=2).sum(axis=0) > 0
    begin = valid_frame.argmax()
    end = len(valid_frame) - valid_frame[::-1].argmax()

    size = end - begin
    bias = random.randint(0, T - size)
    data_shift[:, bias:bias + size, :, :] = data_numpy[:, begin:end, :, :]

    return data_shift


def _rot(rot):
    """
    rot: T,3
    """
    cos_r, sin_r = rot.cos(), rot.sin()  # T,3
    zeros = torch.zeros(rot.shape[0], 1)  # T,1
    ones = torch.ones(rot.shape[0], 1)  # T,1

    r1 = torch.stack((ones, zeros, zeros), dim=-1)  # T,1,3
    rx2 = torch.stack((zeros, cos_r[:, 0:1], sin_r[:, 0:1]), dim=-1)  # T,1,3
    rx3 = torch.stack((zeros, -sin_r[:, 0:1], cos_r[:, 0:1]), dim=-1)  # T,1,3
    rx = torch.cat((r1, rx2, rx3), dim=1)  # T,3,3

    ry1 = torch.stack((cos_r[:, 1:2], zeros, -sin_r[:, 1:2]), dim=-1)
    r2 = torch.stack((zeros, ones, zeros), dim=-1)
    ry3 = torch.stack((sin_r[:, 1:2], zeros, cos_r[:, 1:2]), dim=-1)
    ry = torch.cat((ry1, r2, ry3), dim=1)

    rz1 = torch.stack((cos_r[:, 2:3], sin_r[:, 2:3], zeros), dim=-1)
    r3 = torch.stack((zeros, zeros, ones), dim=-1)
    rz2 = torch.stack((-sin_r[:, 2:3], cos_r[:, 2:3], zeros), dim=-1)
    rz = torch.cat((rz1, rz2, r3), dim=1)

    rot = rz.matmul(ry).matmul(rx)
    return rot


def random_rot(data_numpy, theta=0.3):
    """
    data_numpy: C,T,V,M
    """
    data_torch = torch.from_numpy(data_numpy)
    C, T, V, M = data_torch.shape
    data_torch = data_torch.permute(1, 0, 2, 3).contiguous().view(T, C, V * M)  # T,3,V*M
    rot = torch.zeros(3).uniform_(-theta, theta)
    rot = torch.stack([rot, ] * T, dim=0)
    rot = _rot(rot)  # T,3,3
    data_torch = torch.matmul(rot, data_torch)
    data_torch = data_torch.view(T, C, V, M).permute(1, 0, 2, 3).contiguous()

    return data_torch


def openpose_match(data_numpy):
    C, T, V, M = data_numpy.shape
    assert (C == 3)
    score = data_numpy[2, :, :, :].sum(axis=1)
    # the rank of body confidence in each frame (shape: T-1, M)
    rank = (-score[0:T - 1]).argsort(axis=1).reshape(T - 1, M)

    # data of frame 1
    xy1 = data_numpy[0:2, 0:T - 1, :, :].reshape(2, T - 1, V, M, 1)
    # data of frame 2
    xy2 = data_numpy[0:2, 1:T, :, :].reshape(2, T - 1, V, 1, M)
    # square of distance between frame 1&2 (shape: T-1, M, M)
    distance = ((xy2 - xy1) ** 2).sum(axis=2).sum(axis=0)

    # match pose
    forward_map = np.zeros((T, M), dtype=int) - 1
    forward_map[0] = range(M)
    for m in range(M):
        choose = (rank == m)
        forward = distance[choose].argmin(axis=1)
        for t in range(T - 1):
            distance[t, :, forward[t]] = np.inf
        forward_map[1:][choose] = forward
    assert (np.all(forward_map >= 0))

    # string data
    for t in range(T - 1):
        forward_map[t + 1] = forward_map[t + 1][forward_map[t]]

    # generate data
    new_data_numpy = np.zeros(data_numpy.shape)
    for t in range(T):
        new_data_numpy[:, t, :, :] = data_numpy[:, t, :, forward_map[
                                                             t]].transpose(1, 2, 0)
    data_numpy = new_data_numpy

    # score sort
    trace_score = data_numpy[2, :, :, :].sum(axis=1).sum(axis=0)
    rank = (-trace_score).argsort()
    data_numpy = data_numpy[:, :, :, rank]

    return data_numpy


# 单纯复形
# 计算两点之间的距离的函数
def calculate_distances(points1, points2):
    return torch.sqrt(torch.abs(torch.sum((points1 - points2) ** 2, dim=0)))


# Heron公式计算三角形面积的函数
def calculate_triangle_areas(a, b, c):
    s = (a + b + c) / 2
    area = torch.sqrt(torch.abs(s * (s - a) * (s - b) * (s - c)))
    return area


# 归一化面积张量
def normalize_area(tensor):
    min_val = torch.min(tensor)
    max_val = torch.max(tensor)
    normalized_tensor = (tensor - min_val) / (max_val - min_val)
    return normalized_tensor


# 主函数，计算基于关节点构建的三角形面积和周长的总和，以及坐标的均值
def calculate_features(skeleton_data):
    # 初始化特征张量
    global joint0
    feature_tensor = torch.zeros((3, *skeleton_data.size()[1:]))

    # 定义关节点的选择方案
    # 这里需要根据实际的骨架数据结构来确定关节点的索引
    joint_selection_schemes = {
        # 头部
        'head': [(23, 3, 2)],
        # 颈部
        'neck': [(21, 2, 20)],
        # 腹部
        'abdomen': [(8, 20, 4, 1, 16, 0, 12)],
        # 'abdomen': [(8, 20, 1), (8, 1, 16), (1, 16, 0), (4, 20, 1), (4, 1, 12), (1, 0, 12)],
        # 全身
        'body': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 1, 12, 13, 14, 1, 3, 2, 1)],
        # 'body': [(23, 3, 19), (21, 3, 15)],
        # 四肢
        'arms_and_legs': [(24, 23, 11, 10, 9, 8, 4, 5, 6, 7, 21, 22, 19, 20, 17, 16, 12, 13, 14, 15)],
        # 'arms_and_legs': [(23, 16, 19), (21, 19, 15)],
        # 左右手
        'left_and_right_hands': [(24, 23, 11, 10, 6, 7, 21, 22)],
        # 'left_and_right_hands': [(23, 21, 20)],
        # 左右脚
        'left_and_right_feets': [(19, 18, 15, 14)],
        # 'left_and_right_feets': [(19, 0, 15)],
        # 左右手臂
        'left_and_right_arms': [(10, 9, 8, 4, 5, 6)],
        # 'left_and_right_arms': [(10, 20, 6)],
        # 左右腿
        'left_and_right_thighs': [(17, 16, 13, 12)],
        # 'left_and_right_thighs': [(17, 0, 13)],
        # 左半边身体
        'left_body': [(24, 23, 11, 10, 9, 8, 20, 1, 19, 20, 17, 16, 0)],
        # 'left_body': [(23, 1, 19)],
        # 右半边身体
        'right_body': [(20, 4, 5, 6, 7, 21, 22, 1, 0, 12, 13, 14, 15)],
        # 'right_body': [(21, 1, 15)],
        # 上躯干
        'upper_torso': [(24, 23, 11, 10, 9, 8, 20, 4, 5, 6, 7, 21, 22)],
        # 'upper_torso': [(23, 0, 21)],
        # 左上臂
        'left_upper_arm': [(24, 23, 11, 10, 9, 8, 20)],
        # 'left_upper_arm': [(23, 20, 0)],
        # 右上臂
        'right_upper_arm': [(20, 4, 5, 6, 7, 21, 22)],
        # 'right_upper_arm': [(21, 20, 0)],
        # 左前臂
        'left_forearm': [(24, 23, 11, 10, 9)],
        # 'left_forearm': [(23, 20, 1)],
        # 右前臂
        'right_forearm': [(5, 6, 7, 21, 22)],
        # 'right_forearm': [(21, 20, 1)],
        # 左手
        'left_hand': [(24, 23, 11, 10)],
        # 'left_hand': [(24, 23, 11)],
        # 右手
        'right_hand': [(6, 7, 21, 22)],
        # 'right_hand': [(7, 21, 22)],
        # 下躯干
        'lower_torso': [(19, 18, 17, 16, 0, 12, 13, 14, 15)],
        # 'lower_torso': [(19, 0, 15)],
        # 左大腿
        'left_thigh': [(18, 17, 0)],
        # 'left_thigh': [(18, 17, 0)],
        # 右大腿
        'right_thigh': [(13, 12, 0)],
        # 'right_thigh': [(13, 12, 0)],
        # 左小腿
        'left_calf': [(19, 18, 17)],
        # 'left_calf': [(19, 18, 17)],
        # 右小腿
        'right_calf': [(15, 14, 13)],
        # 'right_calf': [(15, 14, 13)],
        # 左脚
        'left_foot': [(19, 18)],
        # 'left_foot': [(19, 18, 17)],
        # 右脚
        'right_foot': [(15, 14)]
        # 'right_foot': [(15, 14, 13)]
    }

    # 对于每个关节点方案，计算所有帧和所有人的三角形面积和周长
    for joint_part, schemes in joint_selection_schemes.items():
        # 初始化存储面积和周长的变量
        total_area = 0
        total_perimeter = 0
        # 存储构成三角形的所有关节点坐标
        all_points = []

        for (joint0, joint1, joint2) in schemes:
            # 提取关节点坐标
            point0 = skeleton_data[:, :, joint0, :]
            point1 = skeleton_data[:, :, joint1, :]
            point2 = skeleton_data[:, :, joint2, :]
            all_points += [point0, point1, point2]

            # 计算三边长度
            a = calculate_distances(point0, point1)
            b = calculate_distances(point1, point2)
            c = calculate_distances(point0, point2)

            # 计算面积和周长
            total_area += calculate_triangle_areas(a, b, c)
            total_perimeter += a + b + c

        # 计算坐标的均值
        mean_points = torch.mean(torch.stack(all_points), dim=(0, 1))

        # 将计算结果存储在特征张量中
        feature_tensor[0, :, joint0, :] = total_area
        feature_tensor[1, :, joint0, :] = total_perimeter
        feature_tensor[2, :, joint0, :] = mean_points

    return feature_tensor
