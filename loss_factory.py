from typing import Optional, Sequence

import torch
from torch import Tensor
from torch import nn
from torch.nn import functional as F
from timm.loss import LabelSmoothingCrossEntropy, SoftTargetCrossEntropy


class HellingerDistanceLoss(nn.Module):
    def __init__(self):
        super(HellingerDistanceLoss, self).__init__()

    def forward(self, y_true, y_pred):
        """
        计算Hellinger距离损失。

        参数:
        - y_true: 真实标签的概率分布，tensor形式。
        - y_pred: 预测标签的概率分布，tensor形式。

        返回:
        - y_true和y_pred之间的Hellinger距离损失。
        """
        # 确保y_pred不会有0值，因为我们需要计算它们的平方根
        epsilon = 1e-10
        y_pred = torch.clamp(y_pred, min=epsilon, max=1.0)

        # 计算Hellinger距离损失
        loss = torch.sqrt(1 - torch.sum(torch.sqrt(y_true * y_pred), dim=1))
        return torch.mean(loss)


class EMOLoss(nn.Module):
    def __init__(self, cost_matrix=torch.eye(60), lambda_reg=0.1):
        super(EMOLoss, self).__init__()
        self.cost_matrix = cost_matrix
        self.lambda_reg = lambda_reg

    def forward(self, y_pred, y_true):
        """
        计算EMO损失。

        参数:
        - y_pred: 预测的概率分布，形状为(batch_size, num_classes)。
        - y_true: 真实的标签，形状为(batch_size, num_classes)。

        返回:
        - EMO损失值。
        """
        # 计算预测分布和真实分布之间的EMD
        emd_loss = torch.sum(torch.abs(F.softmax(y_pred, dim=1) - y_true) * self.cost_matrix, dim=1)

        # 添加正则项
        reg_loss = self.lambda_reg * torch.sum(y_pred ** 2, dim=1)

        # 总损失
        total_loss = torch.mean(emd_loss + reg_loss)
        return total_loss


# class FocalLoss(nn.Module):
#     """ Focal Loss, as described in https://arxiv.org/abs/1708.02002.
#
#     It is essentially an enhancement to cross entropy loss and is
#     useful for classification tasks when there is a large class imbalance.
#     x is expected to contain raw, unnormalized scores for each class.
#     y is expected to contain class labels.
#
#     Shape:
#         - x: (batch_size, C) or (batch_size, C, d1, d2, ..., dK), K > 0.
#         - y: (batch_size,) or (batch_size, d1, d2, ..., dK), K > 0.
#     """
#
#     def __init__(self,
#                  alpha: Optional[Tensor] = None,
#                  gamma: float = 0.,
#                  reduction: str = 'mean',
#                  ignore_index: int = -100):
#         """Constructor.
#
#         Args:
#             alpha (Tensor, optional): Weights for each class. Defaults to None.
#             gamma (float, optional): A constant, as described in the paper.
#                 Defaults to 0.
#             reduction (str, optional): 'mean', 'sum' or 'none'.
#                 Defaults to 'mean'.
#             ignore_index (int, optional): class label to ignore.
#                 Defaults to -100.
#         """
#         if reduction not in ('mean', 'sum', 'none'):
#             raise ValueError(
#                 'Reduction must be one of: "mean", "sum", "none".')
#
#         super().__init__()
#         self.alpha = alpha
#         self.gamma = gamma
#         self.ignore_index = ignore_index
#         self.reduction = reduction
#
#         self.nll_loss = nn.NLLLoss(
#             weight=alpha, reduction='none', ignore_index=ignore_index)
#
#     def __repr__(self):
#         arg_keys = ['alpha', 'gamma', 'ignore_index', 'reduction']
#         arg_vals = [self.__dict__[k] for k in arg_keys]
#         arg_strs = [f'{k}={v!r}' for k, v in zip(arg_keys, arg_vals)]
#         arg_str = ', '.join(arg_strs)
#         return f'{type(self).__name__}({arg_str})'
#
#     def forward(self, x: Tensor, y: Tensor) -> Tensor:
#         if x.ndim > 2:
#             # (N, C, d1, d2, ..., dK) --> (N * d1 * ... * dK, C)
#             c = x.shape[1]
#             x = x.permute(0, *range(2, x.ndim), 1).reshape(-1, c)
#             # (N, d1, d2, ..., dK) --> (N * d1 * ... * dK,)
#             y = y.view(-1)
#
#         unignored_mask = y != self.ignore_index
#         y = y[unignored_mask]
#         if len(y) == 0:
#             return torch.tensor(0.)
#         x = x[unignored_mask]
#
#         # compute weighted cross entropy term: -alpha * log(pt)
#         # (alpha is already part of self.nll_loss)
#         log_p = F.log_softmax(x, dim=-1)
#         ce = self.nll_loss(log_p, y.long())
#
#         # get true class column from each row
#         all_rows = torch.arange(len(x))
#         log_pt = log_p[all_rows, y]
#
#         # compute focal term: (1 - pt)^gamma
#         pt = log_pt.exp()
#         focal_term = (1 - pt)**self.gamma
#
#         # the full loss: -alpha * ((1 - pt)^gamma) * log(pt)
#         loss = focal_term * ce
#
#         if self.reduction == 'mean':
#             loss = loss.mean()
#         elif self.reduction == 'sum':
#             loss = loss.sum()
#
#         return loss


# def focal_loss(alpha: Optional[Sequence] = None,
#                gamma: float = 0.,
#                reduction: str = 'mean',
#                ignore_index: int = -100,
#                device='cpu',
#                dtype=torch.float32) -> FocalLoss:
#     """Factory function for FocalLoss.
#
#     Args:
#         alpha (Sequence, optional): Weights for each class. Will be converted
#             to a Tensor if not None. Defaults to None.
#         gamma (float, optional): A constant, as described in the paper.
#             Defaults to 0.
#         reduction (str, optional): 'mean', 'sum' or 'none'.
#             Defaults to 'mean'.
#         ignore_index (int, optional): class label to ignore.
#             Defaults to -100.
#         device (str, optional): Device to move alpha to. Defaults to 'cpu'.
#         dtype (torch.dtype, optional): dtype to cast alpha to.
#             Defaults to torch.float32.
#
#     Returns:
#         A FocalLoss object
#     """
#     if alpha is not None:
#         if not isinstance(alpha, Tensor):
#             alpha = torch.tensor(alpha)
#         alpha = alpha.to(device=device, dtype=dtype)
#
#     fl = FocalLoss(
#         alpha=alpha,
#         gamma=gamma,
#         reduction=reduction,
#         ignore_index=ignore_index)
#     return fl


class FocalLoss(nn.Module):
    def __init__(self, alpha=1.5, gamma=2, reduction='mean'):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.reduction = reduction

    def forward(self, inputs, targets):
        BCE_loss = F.cross_entropy(inputs, targets, reduction='none')
        pt = torch.exp(-BCE_loss)
        F_loss = self.alpha * (1 - pt) ** self.gamma * BCE_loss

        if self.reduction == 'mean':
            return torch.mean(F_loss)
        elif self.reduction == 'sum':
            return torch.sum(F_loss)
        else:
            return F_loss



class DMFLoss(nn.Module):
    def __init__(self, alpha=1, gamma=2):
        super(DMFLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma

    def forward(self, inputs, targets):
        # 计算交叉熵损失
        ce_loss = F.cross_entropy(inputs, targets, reduction='none')
        # 计算预测概率
        pt = torch.exp(-ce_loss)
        # 计算组合损失
        loss = self.alpha * ((1 - pt) ** self.gamma) * ce_loss
        return loss.mean()


if __name__ == '__main__':
    # 使用示例
    # 假设我们有一些预测值和真实值
    y_true = torch.tensor([[0, 1], [1, 0]], dtype=torch.float64)
    y_pred = torch.tensor([[0.25, 0.75], [0.6, 0.4]], dtype=torch.float64)

    # 创建损失函数实例
    # hellinger_loss = HellingerDistanceLoss()
    focal_loss = FocalLoss()
    dmf_loss = DMFLoss()

    # 计算损失
    loss = dmf_loss(y_true, y_pred)
    print(loss)
